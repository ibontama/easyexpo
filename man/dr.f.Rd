% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/SL2.r
\name{dr.f}
\alias{dr.f}
\title{DR Dosis Response}
\usage{
dr.f(big = simu, dataset = data.X2, dr = seq(0, 1, 0.1))
}
\arguments{
\item{big}{result of the simulation}

\item{dataset}{dataset with all variables}

\item{dr}{sequence of values that we want to calculate}
}
\value{
a vector
}
\description{
DR Dosis Response
}
\details{
libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"
}
