# Egin nahi ditudan gauzak ####

### descriptive
      library(pacman)
      pacman::p_load(tableone)
      table1 <- CreateTableOne(vars = c('Age', 'Sex', 'Distress'),
                               data = mydata,
                               factorVars = 'Sex',
                               strata = 'Sample')
      table1 <- print(table1,
                      printToggle = FALSE,
                      noSpaces = TRUE)

      library(knitr)
      kable(table1[,1:3],
            align = 'c',
            caption = 'Table 1: Comparison of unmatched samples')

# Besteak

#How is the bigger
bigger.position<-function(vector)
{
  pos<-which(vector %in% max(vector))
  return(pos)
}

# #Ponderate values of variables: 2 numeric + 1 factor
# ponderate.mean<-function(df,r)
# {
#   if(dim(df)[1]==1)
#   {
#     return(df)
#   } else {
#     if (dim(df)[1]==2)
#     {
#       #Hay que ponderar
#       f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2])/2
#       f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2])/2
#       vectorv<-c(df$por2[1],df$por2[2])
#       vectorv2<-vectorv[!is.na(vectorv)]
#       f3<-df[bigger.position(vectorv2),5]
#       n1c<-data.frame(r,"Today",f1,f2,f3,"moba")
#       names(n1c)<-fields
#       return(n1c)
#     } else {
#       if (dim(df)[1]==3)
#       {
#         f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2]+df[3,3]*df$por2[3])/3
#         f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2]+df[3,4]*df$por2[3])/3
#         vectorv<-c(df$por2[1],df$por2[2],df$por2[3])
#         vectorv2<-vectorv[!is.na(vectorv)]
#         f3<-df[bigger.position(vectorv2),5]
#         n1c<-data.frame(r,"Today",f1,f2,f3,"moba")
#         names(n1c)<-fields
#         return(n1c)
#       }
#       else {
#         if (dim(df)[1]==4)
#         {
#           f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2]+df[3,3]*df$por2[3]+df[4,3]*df$por2[4])/4
#           f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2]+df[3,4]*df$por2[3]+df[4,4]*df$por2[4])/4
#           vectorv<-c(df$por2[1],df$por2[2],df$por2[3],df$por2[4])
#           vectorv2<-vectorv[!is.na(vectorv)]
#           f3<-df[bigger.position(vectorv2),5]
#           n1c<-data.frame(r,"Today",f1,f2,f3,"moba")
#           names(n1c)<-fields
#           return(n1c)
#         }
#       }
#     }
#   }
# }



#Solo con 2 numericos
# ponderate.mean ####
ponderate.mean<-function(df,r,ff,fields)
{
  if (ff==21)
  {
    if(dim(df)[1]==1)
    {
      return(df)
    } else {
      if (dim(df)[1]==2)
      {
        #Hay que ponderar
        f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2])/2
        f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2])/2
        vectorv<-c(df$por2[1],df$por2[2])
        vectorv2<-vectorv[!is.na(vectorv)]
        f3<-df[bigger.position(vectorv2),5]
        n1c<-data.frame(r,"Today",f1,f2,f3,"moba")
        names(n1c)<-fields
        return(n1c)
      } else {
        if (dim(df)[1]==3)
        {
          f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2]+df[3,3]*df$por2[3])/3
          f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2]+df[3,4]*df$por2[3])/3
          vectorv<-c(df$por2[1],df$por2[2],df$por2[3])
          vectorv2<-vectorv[!is.na(vectorv)]
          f3<-df[bigger.position(vectorv2),5]
          n1c<-data.frame(r,"Today",f1,f2,f3,"moba")
          names(n1c)<-fields
          return(n1c)
        }
        else {
          if (dim(df)[1]==4)
          {
            f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2]+df[3,3]*df$por2[3]+df[4,3]*df$por2[4])/4
            f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2]+df[3,4]*df$por2[3]+df[4,4]*df$por2[4])/4
            vectorv<-c(df$por2[1],df$por2[2],df$por2[3],df$por2[4])
            vectorv2<-vectorv[!is.na(vectorv)]
            f3<-df[bigger.position(vectorv2),5]
            n1c<-data.frame(r,"Today",f1,f2,f3,"moba")
            names(n1c)<-fields
            return(n1c)
          }
        }
      }
    }
  }

  # 2 numeric values
  if (ff==2)
  {
    if(dim(df)[1]==1)
    {
      return(df)
    } else {
      if (dim(df)[1]==2)
      {
        #Hay que ponderar
        f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2])/2
        f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2])/2
        n1c<-data.frame(r,"Today",f1,f2,"moba")
        names(n1c)<-fields
        return(n1c)
      } else {
        if (dim(df)[1]==3)
        {
          f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2]+df[3,3]*df$por2[3])/3
          f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2]+df[3,4]*df$por2[3])/3
          n1c<-data.frame(r,"Today",f1,f2,"moba")
          names(n1c)<-fields
          return(n1c)
        }
        else {
          if (dim(df)[1]==4)
          {
            f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2]+df[3,3]*df$por2[3]+df[4,3]*df$por2[4])/4
            f2<-(df[1,4]*df$por2[1]+df[2,4]*df$por2[2]+df[3,4]*df$por2[3]+df[4,4]*df$por2[4])/4
            n1c<-data.frame(r,"Today",f1,f2,"moba")
            names(n1c)<-fields
            return(n1c)
          }
        }
      }
    }
  }

  # 1 numeric values
  if (ff==1)
  {
    if(dim(df)[1]==1)
    {
      return(df)
    } else {
      if (dim(df)[1]==2)
      {
        #Hay que ponderar
        f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2])/2
        n1c<-data.frame(r,"Today",f1,"moba")
        names(n1c)<-fields
        return(n1c)
      } else {
        if (dim(df)[1]==3)
        {
          f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2]+df[3,3]*df$por2[3])/3
          n1c<-data.frame(r,"Today",f1,"moba")
          names(n1c)<-fields
          return(n1c)
        }
        else {
          if (dim(df)[1]==4)
          {
            f1<-(df[1,3]*df$por2[1]+df[2,3]*df$por2[2]+df[3,3]*df$por2[3]+df[4,3]*df$por2[4])/4
            n1c<-data.frame(r,"Today",f1,"moba")
            names(n1c)<-fields
            return(n1c)
          }
        }
      }
    }
  }
}


#Data
# dataframe=gs.q
# vector.fields<-c("id","moving_dat","avg_grn_distm","avg_grn_areasqm","grn_within3h","cohort")
# fields<-vector.fields
# fields2<-fields[-2]
# bir<-bir
#
#
# x<-unique.reg(dataframe=gs.q,fields=vector.fields,bir=bir)

#' unique.reg
#' @para dataframe ...
#' @result
#'

# unique.reg ####
unique.reg<-function(dataframe,fields,bir,f)
{

  #noise.q<-unique.reg(dataframe=noise.q,fields=vector.fields,bir=bir,f=1)
  # ...
  #f=21->2 numeric + 1 factor
  #f=2 ->2 numeric
  #dataframe->dataframe with data: id - moving_dat - Different values - cohort
  #fields-> names of fields in a vector; c("id","moving_dat","avg_grn_distm","avg_grn_areasqm","grn_within3h","cohort")
  #movin_dat->debe ser el nombre del campo de fecha
  #bir->a dataframe with id,fert_date,birth_dates
  fields2<-fields[-2]
  lenames<-length(fields)

  #Function
  message("df:", paste(colnames(dataframe), collapse=" "))
  message("f:", paste(fields, collapse=" "))
  dataframe<-dataframe[,fields]
  dataframe$moving_dat<-as.POSIXlt(as.character(dataframe$moving_dat), format="%Y-%m-%d",tz="GMT")

  #More than one geocode
  #Separamos los que solo han salido una vez
  nt<-data.frame(names(table(dataframe$id)),as.vector(table(dataframe$id)))
  names(nt)<-c("id","zenbat")
  head(nt)
  n1<-nt[nt$zenbat==1,1]
  n2<-nt[nt$zenbat>1,1]

  #Donde se guardarán los nuevos valores
  n1b<-data.frame(matrix(888,1,lenames))
  names(n1b)<-fields

  lenn2<-length(n2)
  #AQUI EMPEZARIA EL FOR
  for (q in 1:lenn2)
  {
    cat("\n")
    cat(paste("Numero ",q," de ",lenn2," (%",round(q*100/lenn2,2),")",sep=""))
    cat("\n")

    #Elejimos id y fecha de nacimiento
    #r=id
    r<-as.character(n2[q])
    #Fecha nacimiento
    d.b<-bir$birth_dates[bir$id==r]
    d.b2<-as.POSIXlt(as.character(d.b), format="%Y-%m-%d",tz="GMT")
    # d.b2-Sys.time()

    #Cada dia tiene 86400 segundos
    segd<-86400
    #Mejor sacarlo del FUR
    d.f<-bir$fert_date[bir$id==r]
    d.f2<-as.POSIXlt(as.character(d.f), format="%Y-%m-%d",tz="GMT")
    #Diferencia en dias
    d.bf<-d.b2-d.f2
    #Filtramos por fechas y si queda uno agregamos sino hacemos calculos
    dataframe$por<-0
    ds1<-dataframe[dataframe$id==r,]
    #Orden por fecha
    ds1<-ds1[rev(order(ds1$moving_dat)),]
    lengs<-dim(ds1)[1]
    for (w in 1:lengs)
    {
      ds1$por[w]<-(as.numeric(d.b2-ds1$moving_dat[w]))/as.numeric(d.bf)
    }
    # A veces carecemos de toda la información...por lo que hay que rehacer el cálculo con los datos que tenemos
    suma<-sum(ds1$por)
    if (suma<0.95)
    {
      for (w in 1:lengs)
      {
        ds1$por[w]<-ds1$por[w]/suma
      }
    }

    #Tengo que detectar lo que queda dentro del embarazo
    ds1$por2<-ds1$por
    dd<-dim(ds1)[1]

    # Cuando nos pasamos del valor también reahacer %
    #       if (ds1$por2[dd]==2)
    #       {
    #         ds1$por2[dd]<-1-ds1$por2[1]
    #       }


    #Si solo hay un caso dentro del embarazo
    if (ds1$por[1]>0.999)
    {
      n1b<-rbind(n1b,ds1[1,names(n1b)])

    } else {
      #Casos donde hay un trozo para llegar al 100%
      if (ds1$por[dd]<1.0001){
        zenbat.on<-dd
        n1b<-rbind(n1b,ponderate.mean(ds2,r,f,fields))

      } else {

        if (dd==2)
        {
          ds1$por2[dd]<-1-ds1$por2[1]
        } else {
          if (dd==3)
          {
            ds1$por2[dd]<-1-ds1$por2[2]
          }
          else
          {
            if (dd==4) { ds1$por2[dd]<-1-ds1$por2[3]
            }
          }
        }
        zenbat.on<-as.numeric(table(ds1$por<1)[2]+1)
        ds2<-ds1[1:zenbat.on,]
        # ds2$por2[zenbat.on]<-ds2$por[zenbat.on]-ds2$por[zenbat.on-1]
        n1b<-rbind(n1b,ponderate.mean(ds2,r,f,fields))

      }
    }
  }
  n1b<-n1b[-1,]
  #dataframe<-dataframe[,fields]
  dataframe<-rbind(dataframe[dataframe$id%in%n1,fields2],n1b[fields2])
  return(dataframe)
}



# nire.stats<-function (x, ci = 0.95)
# {
#   y = as.matrix(x)
#   if (is.null(colnames(y))) {
#     Dim = dim(y)[2]
#     if (Dim == 1) {
#       colnames(y) = paste(substitute(x), collapse = ".")
#     }
#     else if (Dim > 1) {
#       colnames(y) = paste(paste(substitute(x), collapse = ""),
#                           1:Dim, sep = "")
#     }
#   }
#   cl.vals = function(x, ci) {
#     x = x[!is.na(x)]
#     n = length(x)
#     if (n <= 1)
#       return(c(NA, NA))
#     se.mean = sqrt(var(x)/n)
#     t.val = qt((1 - ci)/2, n - 1)
#     mn = mean(x)
#     lcl = mn + se.mean * t.val
#     ucl = mn - se.mean * t.val
#     c(lcl, ucl)
#   }
#   nColumns = dim(y)[2]
#   ans = NULL
#   for (i in 1:nColumns) {
#     X = y[, i]
#     X.length = length(X)
#     X = X[!is.na(X)]
#     X.na = X.length - length(X)
#     z = c(X.length, X.na, min(X), max(X), as.numeric(quantile(X,
#                                                               prob = 0.25, na.rm = TRUE)), mean(X), median(X), as.numeric(quantile(X,
#                                                                                                                prob = 0.75, na.rm = TRUE)), sum(X))
#     znames = c("nobs", "NAs", "Minimum", "Maximum", "1. Quartile","Mean", "Median","3. Quartile")
#     result = matrix(z, ncol = 1)
#     row.names(result) = znames
#     ans = cbind(ans, result)
#   }
#   colnames(ans) = colnames(y)
#   data.frame(round(ans, digits = 2))
# }

#' Kutsadura bakoitzaren estadistikoak kalkulatzeko cohorteka
#' Horrela jarraitu

# nire stats ####
nire.stats<-function (x, ci = 0.95)
{
  y = x #x[!is.na(x[,2]),]
  coh<-unique(y$cohort)
  lencoh<-length(coh)
  ans = NULL
  for (i in 1:lencoh) {
    f<-y[y$cohort==coh[i],2]
    X.length = length(f)
    ff = f[!is.na(f)]
    f<-f[!is.na(f)]
    X.na = X.length - length(ff)
    z = c(X.length, X.na, min(f),  as.numeric(quantile(f,prob = 0.25, na.rm = TRUE)), median(f), mean(f), as.numeric(quantile(f, prob = 0.75, na.rm = TRUE)), max(f),sum(f))
    znames = c("nobs", "NAs", "Minimum", "1. Quartile","Median","Mean", "3. Quartile","Maximum", "sum")
    result = matrix(z, ncol = 1)
    row.names(result) = znames
    ans = cbind(ans, result)
  }
  colnames(ans) = coh
  data.frame(round(ans, digits = 2))
}

# basic stats ####
basicStats2<-function (x, ci = 0.95)
{
  y = as.matrix(x)
  if (is.null(colnames(y))) {
    Dim = dim(y)[2]
    if (Dim == 1) {
      colnames(y) = paste(substitute(x), collapse = ".")
    }
    else if (Dim > 1) {
      colnames(y) = paste(paste(substitute(x), collapse = ""),
                          1:Dim, sep = "")
    }
  }
  cl.vals = function(x, ci) {
    x = x[!is.na(x)]
    n = length(x)
    if (n <= 1)
      return(c(NA, NA))
    se.mean = sqrt(var(x)/n)
    t.val = qt((1 - ci)/2, n - 1)
    mn = mean(x)
    lcl = mn + se.mean * t.val
    ucl = mn - se.mean * t.val
    c(lcl, ucl)
  }
  nColumns = dim(y)[2]
  ans = NULL
  for (i in 1:nColumns) {
    X = y[, i]
    X.length = length(X)
    X = X[!is.na(X)]
    X.na = X.length - length(X)
    z = c(X.length, X.na, min(X), as.numeric(quantile(X,prob = 0.25, na.rm = TRUE)),
          median(X), mean(X), as.numeric(quantile(X,prob = 0.75, na.rm = TRUE)),max(X))

    znames = c("nobs", "NAs", "Minimum", "1. Quartile",
               "Median", "Mean", "3. Quartile",  "Maximum")
    result = matrix(z, ncol = 1)
    row.names(result) = znames
    ans = cbind(ans, result)
  }
  colnames(ans) = colnames(y)
  data.frame(round(ans, digits = 6))
}

#' na.table
#' @para dataframe (c) con una variable cohort
#' Elegir city o cohort
#' @result dataframe con %NA
#'

library(plotrix)
# na.table ####
na.table<-function(c,type="City")
{
  #Detecting NAs
  if (type=="City") coh.un<-unique(c$City)
  if (type=="Cohort") coh.un<-unique(c$Cohort)

  len.coh<-length(coh.un)
  len.var<-dim(c)[2]
  df.na<-data.frame(matrix(888,len.var,len.coh))
  rownames(df.na)<-names(c)
  names(df.na)<-coh.un
  i<-0
  j<-0
  #
  #           for(i in 1:len.coh)
  #           { print(coh.un[i])  }
  #           for(j in 1:len.var)
  #           { print(names(c)[j])  }
  #           sapply(final.table,class)
  #           summary(final.table)

  df.na[1:3,]<-0
  #Cities
  for(i in 1:len.coh)
  {
    cat(i)
    cat("\n")
    #Variables
    for (j in 4:len.var)
    {
      ifelse(is.numeric(final.table[,j]),na.pos<-7,na.pos<-3)

      if (type=="Cohort")
      {
        c2<-c[c$Cohort==coh.un[i],j]
        df.na[j,i]<-ifelse(any(is.na(c2)),round(sum(is.na(c[c$City==coh.un[i],j]))*100/dim(c[c$Cohort==coh.un[i],])[1],2),0)
      }
      if (type=="City")
      {
        c2<-c[c$City==coh.un[i],j]
        df.na[j,i]<-ifelse(any(is.na(c2)),round(sum(is.na(c[c$City==coh.un[i],j]))*100/dim(c[c$City==coh.un[i],])[1],2),0)
      }
    }
  }

  #Errezagoa pertsonalizatzeko
  if (type=="City")
  {
    df.na<-df.na[,c("Oslo","Kaunas","Bradford","Nancy","Poitiers","Gipuzkoa","Sabadell","Valencia","Heraklion")]
    mm <- as.matrix(df.na, ncol = 4)
  }

  if (type=="Cohort")
  {
    df.na<-df.na[,c("Moba","Kanc","Bib","Eden","Inma","Rhea")]
    mm <- as.matrix(df.na, ncol = 4)
  }


  par(mar = c(1, 8, 3.5, 1))
  color2D.matplot(mm,
                  show.values = TRUE,
                  axes = FALSE,
                  xlab = "",
                  ylab = "",
                  vcex = 0.5,
                  vcol = "black",
                  extremes = c( "white","red"))
  axis(3, at = seq_len(ncol(mm)) - 0.5,
       labels = colnames(mm), tick = FALSE, cex.axis = 0.7)
  axis(2, at = seq_len(nrow(mm)) -0.5,
       labels = rev(rownames(mm)), tick = FALSE, las = 1, cex.axis = 0.7)
  return(mm)
}

# taula kolorez ####
taulakolorez<-function(mm)
{
  par(mar = c(1, 8, 3.5, 1))
  color2D.matplot(mm,
                  show.values = TRUE,
                  axes = FALSE,
                  xlab = "",
                  ylab = "",
                  vcex = 0.5,
                  vcol = "black",
                  extremes = c( "white","red"))
  axis(3, at = seq_len(ncol(mm)) - 0.5,
       labels = colnames(mm), tick = FALSE, cex.axis = 0.7)
  axis(2, at = seq_len(nrow(mm)) -0.5,
       labels = rev(rownames(mm)), tick = FALSE, las = 1, cex.axis = 0.7)
}

# energetic mean ####
#Energetic mean
energetic.mean2<-function(a)
{
  a2 <- 10^(a/10)
  sabc <- 10 * log10(mean(a2))
  return(sabc)
}

energetic.mean2(c(2,4,3))


#' resetPar
#' @para
#' Para cuando los plots se vuelven locos y para ponerlo por defecto
#' @result pone por defecto el par
#'
# resetpar ####
resetPar <- function() {
  dev.new()
  op <- par(no.readonly = TRUE)
  dev.off()
  op
}

par(resetPar())


# na.table h ####
na.table_h<-function(c,type="h_city")
{
  #Detecting NAs
  c<-c[,sapply(c,is.numeric)]
  if (type=="h_city") coh.un<-unique(c$h_city)
  if (type=="h_cohort") coh.un<-unique(c$Cohort)

  len.coh<-length(coh.un)
  len.var<-dim(c)[2]
  df.na<-data.frame(matrix(888,len.var,len.coh))
  rownames(df.na)<-names(c)
  names(df.na)<-coh.un
  i<-0
  j<-0

  df.na[1:3,]<-0
  #Cities
  for(i in 1:len.coh)
  {
    cat(i)
    cat("\n")
    #Variables
    for (j in 4:len.var)
    {
      print(j)
      # ifelse(is.numeric(final.table[,j]),na.pos<-7,na.pos<-3)

      if (type=="h_cohort")
      {
        c2<-c[c$cohort==coh.un[i],j]
        df.na[j,i]<-ifelse(any(is.na(c2)),round(sum(is.na(c[c$h_cohort==coh.un[i],j]))*100/dim(c[c$h_cohort==coh.un[i],])[1],2),0)
      }
      if (type=="h_city")
      {
        c2<-c[c$h_city==coh.un[i],j]
        df.na[j,i]<-ifelse(any(is.na(c2)),round(sum(is.na(c[c$h_city==coh.un[i],j]))*100/dim(c[c$h_city==coh.un[i],])[1],2),0)
      }
    }
  }

  #Errezagoa pertsonalizatzeko
  if (type=="h_city")
  {
    df.na<-df.na[,c("Oslo","Kaunas","Bradford","Nancy","Poitiers","Gipuzkoa","Sabadell","Valencia","Heraklion")]
    mm <- as.matrix(df.na, ncol = 4)
  }

  if (type=="h_cohort")
  {
    df.na<-df.na[,c("Moba","Kanc","Bib","Eden","Inma","Rhea")]
    mm <- as.matrix(df.na, ncol = 4)
  }


  par(mar = c(1, 8, 3.5, 1))
  color2D.matplot(mm,
                  show.values = TRUE,
                  axes = FALSE,
                  xlab = "",
                  ylab = "",
                  vcex = 0.5,
                  vcol = "black",
                  extremes = c( "white","red"))
  axis(3, at = seq_len(ncol(mm)) - 0.5,
       labels = colnames(mm), tick = FALSE, cex.axis = 0.7)
  axis(2, at = seq_len(nrow(mm)) -0.5,
       labels = rev(rownames(mm)), tick = FALSE, las = 1, cex.axis = 0.7)
  return(mm)
}

# We could use different approaches. He propose the next one:
# 1. Calculate the number of variables that are correlated with others higher than a limit (in our case 0.90)
# 2. Order from highest to lowest number of correlations higher than limit
# 3. Choose the first variable and remove the others highly correlated.
# 4. Remove variables related with the second
# 5. Until no more variable is highly correlated

# Para eliminar las variables Cor > 90
quantity<-function(df.cor,limit)
{
  q<-0
  len.cor<-dim(df.cor)[1]
  for(pos in 1:len.cor){
    dfp<-df.cor[pos,]
    dfp2<-colnames(dfp)[(dfp < (-limit) | dfp > limit) & !is.na(dfp)]

    qq<-as.numeric(unlist(length(dfp2)))
    # print(class(qq))
    q<-c(q,qq)
  }
  #Go removing
  return(q)

}



#Funcion para quitar las "copias" por correlación alta
#We will get a new data frame without the highly correlated variables
remove.high.cor<-function(df.cor,limit)
{
  #Borramos diagonal
  diag(df.cor)<-0
  #contamos var
  len.cor<-dim(df.cor)[1]
  #sacamos los nombres de las var
  names.cor<-rownames(df.cor)
  #limit<-0.89999

  #calculamos cuantos se quedan por debajo del límite
  num.q<-quantity(df.cor,limit)[-1]
  q2<-data.frame(names.cor,num.q)
  q2<-q2[order(-q2$num.q),]
  #Quitamos los que tengan 0
  q2<-q2[q2$num.q!=0,]

  names.df<-rownames(df.cor)
  df.cor2<-df.cor
  len.cor.tx<-dim(q2)[1]-1

  for(w in 1:len.cor.tx)   #len.cor.tx
  {

    name.sel<-as.character(q2$names.cor[w])
    print(name.sel)
    pos2<-which(rownames(df.cor2)==name.sel)

    if(length(pos2)!=0)
    {
      # print("bai")
      #names to remove
      remove.names<-names(df.cor2[pos2,(df.cor2[pos2,] < (-limit) | df.cor2[pos2,] > limit) & !is.na(df.cor2[pos2,])])

      which.2<-which(rownames(df.cor2)%in%remove.names)
      if(length(which.2)!=0)
      {
        #Remove the original
        df.cor2<-df.cor2[-which.2,]
        # names.cor<-names.cor[names.cor%in%remove.names]
      }
    }
  }
  df.cor2<-df.cor2[,which(colnames(df.cor2)%in%rownames(df.cor2))]

  dim(df.cor2)
  return(df.cor2)
}

which.high.cor<-function(df.cor,limit)
{
  #Borramos diagonal
  diag(df.cor)<-0
  #contamos var
  len.cor<-dim(df.cor)[1]
  #sacamos los nombres de las var
  names.cor<-rownames(df.cor)
  #limit<-0.89999

  q<-0
  zen<-0
  len.cor<-dim(df.cor)[1]
  for(pos in 1:len.cor){

    dfp<-df.cor[pos,]
    dfp2<-names(dfp)[(dfp < (-limit) | dfp > limit) & !is.na(dfp)]
    if(length(dfp2)>0)
    {
      zen=zen+1
      print(pos)
      print(colnames(df.cor)[pos])
      print(dfp2)
    }
    print(zen)

    qq<-as.numeric(unlist(length(dfp2)))
    # print(class(qq))
    q<-c(q,qq)
    q<-q[-1]
  }

  #calculamos cuantos se quedan por debajo del límite
  # num.q<-quantity(df.cor,limit)[-1]
  q2<-data.frame(names.cor,q)
  q2<-q2[order(-q2$q),]
  #Quitamos los que tengan 0
  q2<-q2[q2$q!=0,]

  return(q2)
}









#Para dibujar los missings por cohorte
library(plotrix)
na.table.escape<-function(c,type="cohort")
{
  library(raster)
  # c<-data2b[,c(azkena,6:azkena_ken1)]
  #Detecting NAs
  coh.un<-unique(c[,type])

  len.coh<-length(coh.un)
  len.var<-dim(c)[2]
  df.na<-data.frame(matrix(888,len.var,len.coh))
  rownames(df.na)<-names(c)
  names(df.na)<-coh.un
  i<-0
  j<-0

  df.na[1,]<-0
  #Cities
  for(i in 1:len.coh)
  {
    cat(i)
    cat("\n")
    #Variables
    for (j in 2:len.var)
    {
      ifelse(is.numeric(c[,j]),na.pos<-7,na.pos<-3)

      c2<-c[c$cohort==coh.un[i],j]
      df.na[j,i]<-ifelse(any(is.na(c2)),round(sum(is.na(c[c$cohort==coh.un[i],j]))*100/dim(c[c$cohort==coh.un[i],])[1],2),0)
    }
  }

  # #Garbitzen daturik ez dutenak
  # TF.14_18<-grep("_14_18",rownames(df.na))
  # df.na[TF.14_18,c("bib","kanc","nan","poi","rhea")]<-NA
  #
  # TF.3<-grep("_3y",rownames(df.na))
  # df.na[TF.3,c("bib","guip","kanc","nan","poi","rhea","sbd","val")]<-NA
  #
  # TF.7_8<-grep("_7_8y",rownames(df.na))
  # df.na[TF.7_8,c("bib","kanc","nan","poi","rhea")]<-NA
  #
  # TF.birth<-grep("_birth",rownames(df.na))
  # df.na[TF.birth,c("bib","kanc","nan","poi","rhea")]<-NA
  #
  # TF.asq<-grep("_asq|_cdi",rownames(df.na))
  # df.na[TF.asq,c("bib","guip","kanc","moba","rhea","sbd","val")]<-NA
  #
  # TF.mc<-grep("_mc",rownames(df.na))
  # df.na[TF.mc,c("bib","guip","kanc","moba","rhea","sbd","nan","poi")]<-NA
  #
  # TF.bp<-grep("_bp",rownames(df.na))
  # df.na[TF.bp,c("bib","guip","kanc","moba")]<-NA
  #
  # TF.lines<-grep("accesslines|acesslines",rownames(df.na))
  # df.na[TF.lines,c("moba")]<-NA
  #
  # TF.nox<-grep("_nox",rownames(df.na))
  # df.na[TF.nox,c("nan","poi","rhea")]<-NA
  #
  # TF.abs<-grep("25abs",rownames(df.na))
  # df.na[TF.abs,c("nan","poi","guip","val","rhea","bib")]<-NA
  #
  # TF.pm25<-grep("pm25",rownames(df.na))
  # df.na[TF.pm25,c("bib")]<-NA
  #
  # TF.coarse<-grep("pmcoarse",rownames(df.na))
  # df.na[TF.coarse,c("nan","poi","guip","val","bib")]<-NA
  #
  # TF.pm10<-grep("_pm10_",rownames(df.na))
  # df.na[TF.pm10,c("nan","poi","guip","val","bib")]<-NA
  #
  # TF.noise<-grep("_lden_|_ln_",rownames(df.na))
  # df.na[TF.noise,c("guip","val")]<-NA
  #
  # TF.noise2<-grep("_ln_",rownames(df.na))
  # df.na[TF.noise2,]<-NA


  #Para dibujar los missings por cohorte
  #Errezagoa pertsonalizatzeko
  if (type=="cohort")
  {
    df.na<-df.na[,c("bib","guip","kanc","moba","nan","poi","rhea","sbd","val")]
    mm <- as.matrix(df.na[-1,], ncol = 4)
  }


  par(mar = c(1, 8, 3.5, 1))
  color2D.matplot(mm,
                  show.values = TRUE,
                  axes = FALSE,
                  xlab = "",
                  ylab = "",
                  vcex = 0.5,
                  vcol = "black",
                  extremes = c( "white","red"),
                  na.color="grey")
  axis(3, at = seq_len(ncol(mm)) - 0.5,
       labels = colnames(mm), tick = FALSE, cex.axis = 0.7)
  axis(2, at = seq_len(nrow(mm)) -0.5,
       labels = rev(rownames(mm)), tick = FALSE, las = 1, cex.axis = 0.7)

  # r.not20 <- calc(mm, fun=function(x){ x[x != (-1)] <- NA; return(x)} )
  # # plot(r.not20, col="red")
  # # plot(r.20);
  # par(new=TRUE)
  # plot(r.not20, col="grey", legend=FALSE)
  #

  return(mm)
}

#Para dibujar los missings por cohorte
library(plotrix)
na.table.sc<-function(c,type="cohort")
{
  library(raster)
  # c<-data2b[,c(azkena,6:azkena_ken1)]
  #Detecting NAs
  coh.un<-unique(c[,type])

  len.coh<-length(coh.un)
  len.var<-dim(c)[2]
  df.na<-data.frame(matrix(888,len.var,len.coh))
  rownames(df.na)<-names(c)
  names(df.na)<-coh.un
  i<-0
  j<-0

  df.na[1,]<-0
  #Cities
  for(i in 1:len.coh)
  {
    cat(i)
    cat("\n")
    #Variables
    for (j in 2:len.var)
    {
      ifelse(is.numeric(c[,j]),na.pos<-7,na.pos<-3)

      c2<-c[c$cohort==coh.un[i],j]
      df.na[j,i]<-ifelse(any(is.na(c2)),round(sum(is.na(c[c$cohort==coh.un[i],j]))*100/dim(c[c$cohort==coh.un[i],])[1],2),0)
    }
  }

  #Errezagoa pertsonalizatzeko
  if (type=="cohort")
  {
    df.na<-df.na[,c("bib","kanc","moba","poi","rhea","sbd")]
    mm <- as.matrix(df.na[-1,], ncol = 4)
  }


  par(mar = c(1, 8, 3.5, 1))
  color2D.matplot(mm,
                  show.values = TRUE,
                  axes = FALSE,
                  xlab = "",
                  ylab = "",
                  vcex = 0.5,
                  vcol = "black",
                  extremes = c( "white","red"),
                  na.color="grey")
  axis(3, at = seq_len(ncol(mm)) - 0.5,
       labels = colnames(mm), tick = FALSE, cex.axis = 0.7)
  axis(2, at = seq_len(nrow(mm)) -0.5,
       labels = rev(rownames(mm)), tick = FALSE, las = 1, cex.axis = 0.7)

  # r.not20 <- calc(mm, fun=function(x){ x[x != (-1)] <- NA; return(x)} )
  # # plot(r.not20, col="red")
  # # plot(r.20);
  # par(new=TRUE)
  # plot(r.not20, col="grey", legend=FALSE)
  #

  return(mm)
}


#' Kutsadura bakoitzaren estadistikoak kalkulatzeko cohorteka
#' Horrela jarraitu
# nire.stats<-function (x, ci = 0.95)
# {
#   y = x #x[!is.na(x[,2]),]
#   coh<-unique(y$cohort)
#   lencoh<-length(coh)
#   ans = NULL
#   for (i in 1:lencoh) {
#     f<-y[y$cohort==coh[i],2]
#     X.length = length(f)
#     ff = f[!is.na(f)]
#     f<-f[!is.na(f)]
#     X.na = X.length - length(ff)
#     z = c(X.length, X.na, min(f),  as.numeric(quantile(f,prob = 0.25, na.rm = TRUE)), median(f), mean(f), as.numeric(quantile(f, prob = 0.75, na.rm = TRUE)), max(f),sum(f))
#     znames = c("nobs", "NAs", "Minimum", "1. Quartile","Median","Mean", "3. Quartile","Maximum", "sum")
#     result = matrix(z, ncol = 1)
#     row.names(result) = znames
#     ans = cbind(ans, result)
#   }
#   colnames(ans) = coh
#   data.frame(round(ans, digits = 2))
# }

basicStats2<-function (x, ci = 0.95)
{
  y = as.matrix(x)
  if (is.null(colnames(y))) {
    Dim = dim(y)[2]
    if (Dim == 1) {
      colnames(y) = paste(substitute(x), collapse = ".")
    }
    else if (Dim > 1) {
      colnames(y) = paste(paste(substitute(x), collapse = ""),
                          1:Dim, sep = "")
    }
  }

cl.vals = function(x, ci) {
    x = x[!is.na(x)]
    n = length(x)
    if (n <= 1)
      return(c(NA, NA))
    se.mean = sqrt(var(x)/n)
    t.val = qt((1 - ci)/2, n - 1)
    mn = mean(x)
    lcl = mn + se.mean * t.val
    ucl = mn - se.mean * t.val
    c(lcl, ucl)
  }
  nColumns = dim(y)[2]
  ans = NULL
  for (i in 1:nColumns) {
    X = y[, i]
    X.length = length(X)
    X = X[!is.na(X)]
    X.na = X.length - length(X)
    z = c(X.length, X.na, min(X), as.numeric(quantile(X,prob = 0.25, na.rm = TRUE)),
          median(X), mean(X), as.numeric(quantile(X,prob = 0.75, na.rm = TRUE)),max(X))

    znames = c("nobs", "NAs", "Minimum", "1. Quartile",
               "Median", "Mean", "3. Quartile",  "Maximum")
    result = matrix(z, ncol = 1)
    row.names(result) = znames
    ans = cbind(ans, result)
  }
  colnames(ans) = colnames(y)
  data.frame(round(ans, digits = 6))
}

#' na.table
#' @para dataframe (c) con una variable cohort
#' Elegir city o cohort
#' @result dataframe con %NA
#'
#'
#'         #########
#'

##################################################################################

multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  require(grid)

  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)

  numPlots = length(plots)

  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }

  if (numPlots==1) {
    print(plots[[1]])

  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))

    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))

      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}


#Funcion
p.anova<-function(formula)
{
  formula2<-as.formula(formula)
  fit <- aov(formula2, data=final.table.imp.unt.TT)
  summary(fit) # display Type I ANOVA table
  pv<-as.numeric(summary(fit)[[1]][[5]][1])
  if (pv<0.001) pv<-"<0.0001"
  if (pv>0.001) pv<-round(pv,3)
  return(pv)
}

#Funcion
minmax<-function(dd)
{
  rg<-range(dd[,2],na.rm =TRUE)
  rg2<-round(rg[2]-rg[1],2)
  return(rg2)
}


#Function for 4 plotting
plot4<-function(exposure,xrot)
{
  pv<-p.anova(paste(exposure," ~ City",sep=""))

  num<-which(names(final.table.imp.unt.TT) == exposure)
  dd<-aggregate(x =final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT[,num]),num], by = list(GD=final.table.imp.unt.TT$City[!is.na(final.table.imp.unt.TT[,num])]), FUN = "mean")
  dif<-minmax(dd)

  p1<-ggplot(final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT$City),],aes_string(x="City",y=exposure)) + geom_boxplot()  + #coord_flip() +
    labs(x = "",y = xrot) +
    theme(legend.position="none",panel.background = element_rect(fill = 'white', colour = 'black'))+theme_bw()+
    ggtitle(paste("By City - p-value: ",pv," dif:",dif,sep=""))

  #pv<-p.anova("NO2 ~ Cohort")
  pv<-p.anova(paste(exposure," ~ Cohort",sep=""))

  dd<-aggregate(x =final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT[,num]),num], by = list(GD=final.table.imp.unt.TT$Cohort[!is.na(final.table.imp.unt.TT[,num])]), FUN = "mean")
  dif<-minmax(dd)

  p2<-ggplot(final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT$Cohort),],aes_string(x="Cohort",y=exposure)) + geom_boxplot()  + #coord_flip() +
    labs(x = "",y = xrot) +
    theme(legend.position="none",panel.background = element_rect(fill = 'white', colour = 'black'))+theme_bw()+
    ggtitle(paste("By Cohort - p-value: ",pv," dif:",dif,sep=""))

  # }
  #pv<-p.anova("NO2 ~ MotherEducation")
  pv<-p.anova(paste(exposure," ~ MotherEducation",sep=""))

  num<-which(names(final.table.imp.unt.TT) == exposure)
  dd<-aggregate(x =final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT[,num]),num], by = list(GD=final.table.imp.unt.TT$MotherEducation[!is.na(final.table.imp.unt.TT[,num])]), FUN = "mean")
  dif<-minmax(dd)

  p3<-ggplot(final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT$MotherEducation),],aes_string(x="MotherEducation",y=exposure)) + geom_boxplot()  + #coord_flip() +
    labs(x = "",y = xrot) +
    theme(legend.position="none",panel.background = element_rect(fill = 'white', colour = 'black'))+theme_bw()+
    ggtitle(paste("By Education level - p-value: ",pv," dif:",dif,sep=""))

  #pv<-p.anova("NO2 ~ FromCountry")
  pv<-p.anova(paste(exposure," ~ FromCountry",sep=""))

  dd<-aggregate(x =final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT[,num]),num], by = list(GD=final.table.imp.unt.TT$FromCountry[!is.na(final.table.imp.unt.TT[,num])]), FUN = "mean")
  dif<-minmax(dd)

  p4<-ggplot(final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT$FromCountry),],aes_string(x="FromCountry",y=exposure)) + geom_boxplot()  + #coord_flip() +
    labs(x = "",y = xrot) +
    theme(legend.position="none",panel.background = element_rect(fill = 'white', colour = 'black'))+theme_bw()+
    ggtitle(paste("By 'Mother born in Cohort Country' - p-value: ",pv," dif:",dif,sep=""))

  multiplot(p1, p2, p3,p4, cols=2)
}


korplot<-function(c.cor,title2)
{
  is.na(c.cor)<-0
  diag(c.cor)<-0

  cex.before <- par("cex")
  par(cex = 0.4)
  corrplot(c.cor, method="shade", shade.col=NA, tl.col="black",
           tl.srt=45,addCoef.col="black",
           order="original",type="upper",title=title2)
  par(cex = cex.before)
}


### ESTANDARIZING WITH IQR global
ft<-data.frame(exposures2[,!sapply(exposures2,is.numeric)],
               exposures2[,sapply(exposures2,is.numeric)])
names(ft)
dv<-dim(ft)[2]
un.c<-unique(ft$h_cohort.x)
len.un.c<-length(un.c)


for (i in 5:dv)
{
  print(i)
  var.iqr<-IQR(ft[,i],na.rm = TRUE)
  ft[!is.na(ft[,i]),i]<-ft[!is.na(ft[,i]),i]/var.iqr

}
exposures2.est.IQR.global<-ft
save(exposures2.est.IQR.global,file="exposures2.est.IQR.global.RData")

exposures2<-exposures2.est.IQR.global


#Mean centered by cohort

ft<-data.frame(exposures2[,!sapply(exposures2,is.numeric)],
               exposures2[,sapply(exposures2,is.numeric)])

names(ft)
dv<-dim(ft)[2]
un.c<-unique(ft$h_cohort.x)
len.un.c<-length(un.c)

for (i in 5:dv)
{
  for (k in 1:len.un.c)
  {
    print(names(ft)[i])
    print (un.c[k])
    av<-mean(ft[ft$h_cohort.x==un.c[k],i],na.rm=TRUE)
    ft[ft$h_cohort.x==un.c[k],i]<-ft[ft$h_cohort.x==un.c[k],i]-av
  }
}
exposures2.cen<-ft

## mcen + scaled

exposures.cen.scale<-data.frame(exposures2.cen[,!sapply(exposures2.cen,is.numeric)],
                                scale(exposures2.cen[,sapply(exposures2.cen,is.numeric)],
                                      center = FALSE, scale = TRUE))



#### No mean centered

exposures.nocen.scale<-data.frame(exposures2[,!sapply(exposures2,is.numeric)],
                                  scale(exposures2[,sapply(exposures2,is.numeric)],
                                        center = FALSE, scale = TRUE))


