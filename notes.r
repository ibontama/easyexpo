devtools::document()
# install.packages("formatR")
formatR::tidy_dir("R")
# install.packages("lintr")
lintr::lint_package()

ls("package:easyexpo")

purl("test.Rmd")

devtools::install_bitbucket("ibontama/easyexpo")

devtools::use_package("MASS")
devtools::use_package("mice")
devtools::use_package("stats")
devtools::use_package("xlsx")
devtools::use_package("gridExtra")
devtools::use_package("plotrix")
devtools::use_package("raster")
devtools::use_package("corrplot")
devtools::use_package("gridExtra")
devtools::use_package("SuperLearner")
devtools::use_package("")
devtools::use_package("")

# rdb is corrupt
.rs.restartR()

# descriptiboak egiteko pakete ezberdinak baina onenak

# summarytools
# https://cran.r-project.org/web/packages/summarytools/vignettes/Introduction.html?platform=hootsuite
# DescTools
# https://dabblingwithdata.wordpress.com/2018/01/02/my-favourite-r-package-for-summarising-data/
#


# A package should appear in just one of these four sections (Depends, Imports, Suggests, and Enhances).
#
# If you need to attach the package with library(): Depends
# If you use functions from the package (::, @import, or @importFrom) but don’t need to use library(): Imports
# If it’s not used in the code but is used in examples, vignettes, or tests: Suggests
# Otherwise maybe Enhances


#Debugging
# Para ir paso a paso por una función utilizar debug()
debug(mice)
undebug(mice)

# Para ver y poder guardar los cambios en una función utilizar fix()
fix(mice)

# Para ver las funciones que están dentro de un paquete utilizar mice:::funciones
mice:::updateLog()



devtools::install_github("carleshf/rexposome", ref="devel")

https://isglobal-brge.github.io/rexposome/

multinomial regression (logistic)


mod <- glm(..., family=family, formula = frm, data = dta)


mod0 <- update(mod, as.formula(paste0(". ~ . - ", all.vars(frm)[2])))

                effect <- c(mod$coef[2], suppressMessages(confint.default(mod)[2,]))
                p <- anova(mod, mod0, test = test)
                p2 <- p[[names(p)[length(names(p))]]][2] # `Pr(>F)`, `Pr(>Chi)`
                return(c(effect, p2))



# # You need the suggested package for this function
# my_fun <- function(a, b) {
#   if (!requireNamespace("pkg", quietly = TRUE)) {
#     stop("Pkg needed for this function to work. Please install it.",
#          call. = FALSE)
#   }
# }
#
# # There's a fallback method if the package isn't available
# my_fun <- function(a, b) {
#   if (requireNamespace("pkg", quietly = TRUE)) {
#     pkg::f()
#   } else {
#     g()
#   }
# }
#
# # Imports:
# #   ggvis (>= 0.2),
# # dplyr (>= 0.3.0.1)
# # Suggests:
# #   MASS (>= 7.3.0)
#
#
# # Cuando necesito un paquete
# #import
# devtools::use_package("dplyr")
# #suggest
# devtools::use_package("mgcv", "Suggests")
#
