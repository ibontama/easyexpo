#' Calculate how many variables are under the correlation limit
#'
#' @param df.cor correlation matrix
#' @param lim Limite choosen to asses the relation
#' @return The number of variables
#' @export

pr.limit<-function(df.cor,lim)
{
  num.cor<-(length(df.cor)^2)/2
  bigger.cor<-as.numeric(table(ifelse(abs(df.cor)>lim,1,0))[names(table(ifelse(abs(df.cor)>lim,1,0)))=="1"])/2

  pr.cor<-round(bigger.cor*100/num.cor,2)
  return(pr.cor)
}

#' Plot the number of correlations that would be under the correlation limit
#'
#' @param var A vector with number values.
#' @param df.cor Correlation matrix
#' @return A plot with the relation between correlation limit and the number of variables
#' @export

plot_cor_lim<-function(df.cor)
{
  sek<-seq(from=95,to=5,by=-5)/100
  pr.cor<-sapply(sek,function(x)
  {
    pr.limit(df.cor,x)
  })

  df.lim.cor<-data.frame(sek,pr.cor)

  plot(df.lim.cor,type="l")
}

