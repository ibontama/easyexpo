#' Impute the missing values of the dataset
#'
#' @param expo The dataset with the variables
#' @param m Number of imputed datasets
#' @param lim Number of variables to predict the variables
#' @param include.var The variables that we want to include always in the imputation
#' @param exclude.var Vector with the name of variables that we want not include
#' @param outcome The name of the outcome
#' @return imp Imputed datasets. The idea is to avoid the imputing from highly correlated variables.
#' @export
impute_expo <- function(expo, m, lim = 10, include.var = c("h_cohort.x"), exclude.var = "HelixID", outcome) {
    
    include.var <- c(include.var, outcome)
    
    exp.num <- expo[, sapply(expo, is.numeric)]
    
    df.cor <- as.data.frame(stats::cor(exp.num[, -which(names(exp.num) %in% outcome)], use = "pairwise.complete.obs"))
    diag(df.cor) <- 0
    
    pred <- mice::quickpred(expo, mincor = 0.4, minpuc = 0.4, include = include.var, exclude = exclude.var)
    
    pred2 <- matrix_filter(pred, n = lim, df.cor, 2)
    
    # Make high correlated variables 0
    pred2[, grep("_t1|_t2|_t3", colnames(pred2), value = TRUE)] <- 0
    pred2[, grep("100_|500_", colnames(pred2), value = TRUE)] <- 0
    
    imp <- mice::mice(expo, pred = pred2, seed = 38788, m = m, maxit = 10)
    
    return(imp)
}


#' Filter the prediction matrix with n number of variables
#'
#' @param pred Prediction matrix
#' @param n Number of variables that we want to limit to
#' @param df.cor Correlation matrix
#' @param incl How many variables we want to force to be (numeric)
#' @return Prediction matrix with less correlations
#' @export
matrix_filter <- function(pred, n, df.cor, incl) {
    # extraer los 1 de cada uno
    len.pred <- dim(pred)[1]
    for (t in 1:len.pred) {
        print(t)
        print(colnames(pred)[t])
        names.1 <- names(pred[colnames(pred)[t], pred[colnames(pred)[t], ] == 1])
        # extraemos las correlaciones
        dfb <- data.frame(df.cor[names.1, colnames(pred)[t]], names.1)
        if (dim(dfb)[1] > 0) {
            names(dfb) <- c("Correlation", "Variable")
            # quitamos NAs
            dfb <- dfb[!is.na(dfb$Correlation), ]
            n3 <- dim(dfb)[1]
            # Ponemos valores absolutos
            dfb$Correlation <- abs(dfb$Correlation)
            # Eliminamos los que tienen más de 0.90
            dfb <- dfb[dfb$Correlation < 0.9, ]
            # Ordenamos por correlación
            dfb <- dfb[order(-dfb$Correlation), ]
            
            
            # convertir los demás restantes en 0 en la matriz Si son menos podemos dejarlo como está
            if (n3 >= n) {
                n2 <- n + 1 - incl
                convert0 <- as.character(dfb[n2:n3, 2])
                convert0 <- convert0[!is.na(convert0)]
                # nos quedamos con n variables
                n.n <- ifelse(n3 < n, n3, n)
                dfb <- dfb[1:n.n, ]
                # Convertir en 0
                pred[colnames(pred)[t], convert0] <- 0
            }
            
        }
        
        
    }
    return(pred)
}

