#' Plot 9 histogramas with different normalization
#'
#' @param var A vector with number values.
#' @param nam The name of the variable.
#' @return A plot with 9 histograms where the last one is the best solution based on boxcox indeces.
#' @examples
#' plot_norm(mtcars$mpg,'MPG')
#' @export
plot_norm <- function(var, nam)
  {
    d <- var[!is.na(var)]

    if (min(d) <= 0) {
        mind<-min(d[d>0])
        mind2<-abs(min(d))+mind+0.001
        d<-d+mind2
        # hist(d, main = nam)
    }
    # else {
    #     if (min(d) == 0) {
    #         d <- d + 1e-06
    #     }
        dd <- best_trans(d)
        layout(matrix(c(1, 2, 3, 4, 5, 6, 7, 8, 9), 3, 3, byrow = TRUE))
        hist(1/(d^2), main = "1/Y2 (-2)", breaks = 20, col = "blue", ylab = "", xlab = "")
        hist(1/(d), main = "1/Y1 (-1)", breaks = 20, col = "red", ylab = "", xlab = "")
        hist(1/sqrt(d), main = "1/(Sqrt(Y)) (-0.5)", breaks = 20, col = "orange", ylab = "", xlab = "")
        hist(log(d), main = "log(Y) (0)", breaks = 20, col = "green", ylab = "", xlab = "")
        hist(sqrt(d), main = "sqrt(Y) (0.5)", breaks = 20, col = "yellow", ylab = "", xlab = "")
        hist(d, main = "Y (1)", breaks = 20, col = "black", ylab = "", xlab = "")
        hist(d^2, main = "Y2 (2)", breaks = 20, col = "grey", ylab = "", xlab = "")
        hist(d^(1/3), main = "^1/3", breaks = 20, col = "purple", ylab = "", xlab = "")
        hist(dd, main = paste0("best-", best_trans_name(d), "-", nam), breaks = 20, col = "red", ylab = "", xlab = "")
    # }
}

#' Choose the best transformation based on boxcox
#'
#' @param var A vector with numeric variables
#' @return The best transformed variable based on boxcox index
#' @examples
#' best_trans(mtcars$mpg)
#' @export
best_trans <- function(var) {
    bc <- MASS::boxcox(var ~ 1, plotit = FALSE)
    trans <- bc$x[which.max(bc$y)]

    if (trans < (-1.5)) {
        x <- 1/(var^2)
    } else if (trans < (-0.75) & trans > (-1.5)) {
        x <- 1/(var)
    } else if (trans < (-0.25) & trans > (-0.75)) {
        x <- 1/(sqrt(var))
    } else if (trans < (0.25) & trans > (-0.25)) {
        x <- log(var)
    } else if (trans < (0.75) & trans > (0.25)) {
        x <- sqrt(var)
    } else if (trans < (1.5) & trans > (0.75)) {
        x <- var
    } else {
        x <- var^2
    }

    return(x)
}


#' Show the best transformation method based on boxcox
#'
#' @param var A vector with numeric variables
#' @return The best transformed variable based on boxcox index
#' @examples
#' best_trans_name(mtcars$mpg)
#' @export
best_trans_name <- function(var) {
    bc <- MASS::boxcox(var ~ 1, plotit = FALSE)
    trans <- bc$x[which.max(bc$y)]

    x <- ifelse(trans < (-1.5), "1/(var^2)", ifelse(trans < (-0.75) & trans > (-1.5), "1/(var)", ifelse(trans < (-0.25) & trans >
        (-0.75), "1/(sqrt(var))", ifelse(trans < (0.25) & trans > (-0.25), "log(var)", ifelse(trans < (0.75) & trans > (0.25),
        "sqrt(var)", ifelse(trans < (1.5) & trans > (0.75), "var", "var^2"))))))
    return(x)
}

#' Obtain the date or date+hour of the moment
#'
#' @param time1 day, hour or day_hour
#' @return Day, hour or both
#' @export
#'
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  require(grid)

  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)

  numPlots = length(plots)

  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }

  if (numPlots==1) {
    print(plots[[1]])

  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))

    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))

      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}


#' Obtain the date or date+hour of the moment
#'
#' @param time1 day, hour or day_hour
#' @return Day, hour or both
#' @export
#'
p.anova<-function(formula)
{
  formula2<-as.formula(formula)
  fit <- aov(formula2, data=final.table.imp.unt.TT)
  summary(fit) # display Type I ANOVA table
  pv<-as.numeric(summary(fit)[[1]][[5]][1])
  if (pv<0.001) pv<-"<0.0001"
  if (pv>0.001) pv<-round(pv,3)
  return(pv)
}
#' Show the best transformation method based on boxcox
#'
#' @param var A vector with numeric variables
#' @return The best transformed variable based on boxcox index
#'
#' @export
#'
#Function for 4 plotting
plot4<-function(exposure,xrot)
{
  pv<-p.anova(paste(exposure," ~ City",sep=""))

  num<-which(names(final.table.imp.unt.TT) == exposure)
  dd<-aggregate(x =final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT[,num]),num], by = list(GD=final.table.imp.unt.TT$City[!is.na(final.table.imp.unt.TT[,num])]), FUN = "mean")
  dif<-minmax(dd)

  p1<-ggplot(final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT$City),],aes_string(x="City",y=exposure)) + geom_boxplot()  + #coord_flip() +
    labs(x = "",y = xrot) +
    theme(legend.position="none",panel.background = element_rect(fill = 'white', colour = 'black'))+theme_bw()+
    ggtitle(paste("By City - p-value: ",pv," dif:",dif,sep=""))

  #pv<-p.anova("NO2 ~ Cohort")
  pv<-p.anova(paste(exposure," ~ Cohort",sep=""))

  dd<-aggregate(x =final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT[,num]),num], by = list(GD=final.table.imp.unt.TT$Cohort[!is.na(final.table.imp.unt.TT[,num])]), FUN = "mean")
  dif<-minmax(dd)

  p2<-ggplot(final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT$Cohort),],aes_string(x="Cohort",y=exposure)) + geom_boxplot()  + #coord_flip() +
    labs(x = "",y = xrot) +
    theme(legend.position="none",panel.background = element_rect(fill = 'white', colour = 'black'))+theme_bw()+
    ggtitle(paste("By Cohort - p-value: ",pv," dif:",dif,sep=""))

  # }
  #pv<-p.anova("NO2 ~ MotherEducation")
  pv<-p.anova(paste(exposure," ~ MotherEducation",sep=""))

  num<-which(names(final.table.imp.unt.TT) == exposure)
  dd<-aggregate(x =final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT[,num]),num], by = list(GD=final.table.imp.unt.TT$MotherEducation[!is.na(final.table.imp.unt.TT[,num])]), FUN = "mean")
  dif<-minmax(dd)

  p3<-ggplot(final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT$MotherEducation),],aes_string(x="MotherEducation",y=exposure)) + geom_boxplot()  + #coord_flip() +
    labs(x = "",y = xrot) +
    theme(legend.position="none",panel.background = element_rect(fill = 'white', colour = 'black'))+theme_bw()+
    ggtitle(paste("By Education level - p-value: ",pv," dif:",dif,sep=""))

  #pv<-p.anova("NO2 ~ FromCountry")
  pv<-p.anova(paste(exposure," ~ FromCountry",sep=""))

  dd<-aggregate(x =final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT[,num]),num], by = list(GD=final.table.imp.unt.TT$FromCountry[!is.na(final.table.imp.unt.TT[,num])]), FUN = "mean")
  dif<-minmax(dd)

  p4<-ggplot(final.table.imp.unt.TT[!is.na(final.table.imp.unt.TT$FromCountry),],aes_string(x="FromCountry",y=exposure)) + geom_boxplot()  + #coord_flip() +
    labs(x = "",y = xrot) +
    theme(legend.position="none",panel.background = element_rect(fill = 'white', colour = 'black'))+theme_bw()+
    ggtitle(paste("By 'Mother born in Cohort Country' - p-value: ",pv," dif:",dif,sep=""))

  multiplot(p1, p2, p3,p4, cols=2)
}




