#' Calculate how many variables are under the correlation limit
#'
#' @param df.cor correlation matrix
#' @param lim Limite choosen to asses the relation
#' @return The number of variables
#' @export

pr_limit <- function(df.cor, lim) {
    # num.cor <- (length(df.cor)^2)/2
    num.cor <- (length(df.cor)-dim(df.cor)[1])/2
    bigger.cor <- as.numeric(table(ifelse(abs(df.cor) > lim, 1, 0))[names(table(ifelse(abs(df.cor) > lim, 1, 0))) == "1"])/2

    pr.cor <- round(bigger.cor * 100/num.cor, 2)
    return(pr.cor)
}

#' Plot the number of correlations that would be under the correlation limit
#'
#' @param var A vector with number values.
#' @param df.cor Correlation matrix
#' @return A plot with the relation between correlation limit and the number of variables
#' @export

plot_cor_lim <- function(df.cor) {
    sek <- seq(from = 95, to = 5, by = -1)/100
    pr.cor <- sapply(sek, function(x) {
        pr_limit(df.cor, x)
    })

    df.lim.cor <- data.frame(sek, pr.cor)

    plot(df.lim.cor, type = "l",ylab="% of variables under the limit",xlab="Limit correlation",col=2,
         main="% of variables under the limit")
}

#' Detect the variables that are correlated with each of the variables
#'
#' @param df.cor Correlation matrix
#' @param limit the limit of correlation
#' @return Three tables: with all the correlations bigger than limit, which is not correlated with no one and who is relate with someone
#' @export

which_high_cor <- function(df.cor, limit) {
    limit <- limit
    # Borramos diagonal
    diag(df.cor) <- 0
    # contamos var
    len.cor <- dim(df.cor)[1]
    # sacamos los nombres de las var
    names.cor <- rownames(df.cor)
    # limit<-0.89999

    q <- 0
    zen <- 0

    df.var.cor <- c("", "", "", "")

    len.cor <- dim(df.cor)[1]
    for (pos in 1:len.cor) {

        dfp <- df.cor[pos, ]
        names.var <- names.cor[pos]
        dfp2 <- names(dfp)[abs(dfp) > limit & !is.na(dfp)]
        len.dfp2 <- length(dfp2)

        if (len.dfp2 > 0) {
            zen = zen + 1
            vec.var <- c(names.var, len.dfp2, "", "")

            # ent<-len.dfp2%/%4
            hon <- len.dfp2%%4

            if (hon == 1) {
                vec.cor <- c(dfp2, "", "", "")
            } else if (hon == 2) {
                vec.cor <- c(dfp2, "", "")
            } else if (hon == 3) {
                vec.cor <- c(dfp2, "")
            } else {
                vec.cor <- dfp2
            }

            hutsa <- c("", "", "", "")

            df.var.cor <- c(df.var.cor, vec.var, vec.cor, hutsa)
            # Datu hauek gorde behar dira dataframean print(pos) print(colnames(df.cor)[pos]) print(dfp2)

        }
        qq <- as.numeric(unlist(length(dfp2)))
        q <- c(q, qq)
        # print(zen) print(class(qq))
    }

    q <- q[-1]
    # calculamos cuantos se quedan por debajo del límite num.q<-quantity(df.cor,limit)[-1]
    q2 <- data.frame(names.cor, q)
    q2 <- q2[order(-q2$q), ]
    names(q2) <- c("Variable", "HighCorrelated")
    # Quitamos los que tengan 0
    q3 <- q2[q2$HighCorrelated != 0, ]

    erreg <- length(df.var.cor)/4
    taula2 <- data.frame(matrix(df.var.cor, erreg, 4, byrow = TRUE))
    taula2 <- taula2[-1, ]
    names(taula2) <- c("Variable", "NCorr", "-", "-")

    return(list(all = q2, without0 = q3, table = taula2))
}

#' Export the tables in excel and pdf
#'
#' @param cor.q Correlation matrix
#' @param limit the limit of correlation
#' @return Excel and PDF files with the information with correlations
#' @export

export_table_cor <- function(cor.q, limit) {
    lst <- which_high_cor(cor.q, limit = 0.3)
    # names(lst)

    day <- easyexpo::day_hour("day")

    ifelse(!dir.exists("CorrelationTables/"), dir.create("CorrelationTables/"), FALSE)

    # All correlations
    all.cor <- lst[[1]]
    xlsx::write.xlsx(all.cor, paste0("CorrelationTables/", day, "_All_number_of_correlations.xlsx", sep = ""))
    p <- gridExtra::tableGrob(all.cor)
    ggsave(filename = paste0("CorrelationTables/", day, "_All_number_of_correlations.pdf", sep = ""), plot = grid.arrange(p),
        scale = 1, width = 28, height = 20, units = "cm")

    # Without zeros
    nozeros <- lst[[2]]
    xlsx::write.xlsx(nozeros, paste0("CorrelationTables/", day, "_WithoutZeros.xlsx", sep = ""))
    p <- gridExtra::tableGrob(nozeros)
    ggsave(filename = paste0("CorrelationTables/", day, "_WithoutZeros.pdf", sep = ""), plot = grid.arrange(p), scale = 1,
        width = 28, height = 20, units = "cm")

    # Correlation with variables
    taula <- lst[[3]]

    xlsx::write.xlsx(taula, paste0("CorrelationTables/", day, "_All_Variables.xlsx", sep = ""))
    p <- gridExtra::tableGrob(taula)
    ggsave(filename = paste0("CorrelationTables/", day, "_All_Variables.pdf", sep = ""), plot = grid.arrange(p), scale = 1,
        width = 28, height = 20, units = "cm")

    # table(ifelse(all.cor$HighCorrelated==0,0,1))
    # table(ifelse(all.cor$HighCorrelated==0,0,ifelse(all.cor$HighCorrelated==1,1,2)))


}
