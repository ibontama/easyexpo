#' Create a correlation plot
#'
#' @param c.cor Data frame
#' @param title2 Name of the plot
#' @return A plot
#' @export

korplot <- function(c.cor, title2) {
    is.na(c.cor) <- 0
    diag(c.cor) <- 0

    cex.before <- par("cex")
    par(cex = 0.4)
    corrplot::corrplot(c.cor, method = "shade", shade.col = NA, tl.col = "black", tl.srt = 45, addCoef.col = "black", order = "original",
        type = "full", title = title2)
    par(cex = cex.before)
    # corrplot(df.cor, method='square',col = col3(100), tl.col=1,tl.cex=1.4,tl.srt=90)
}


#' Clean the names from noisy oens (remove h_, _preg...)
#'
#' @param variables vector with the names
#' @return a vector without noise
#' @export
#'
clean_names<-function(variables)
{
  f2=gsub("_ratio","",variables,fixed=T)
  f2=gsub("h_","",f2,fixed=T)
  f2=gsub("hs_","",f2,fixed=T)
  f2=gsub("_t1","",f2,fixed=T)
  f2=gsub("_t2","",f2,fixed=T)
  f2=gsub("_t3","",f2,fixed=T)
  f2=gsub("_preg","",f2,fixed=T)
  f2=gsub("100","",f2,fixed=T)
  f2=gsub("300","",f2,fixed=T)
  f2=gsub("500","",f2,fixed=T)
  return(f2)
}


#' Displays the correlation matrix with lines at the specified locations (for exposures families)
#'
#' @param corr correlation matrix
#' @param families family of each variable
#' @param variables name of the variables
#' @param save name of the pdf
#' @param displ.var if we want to show all (all) variables names or just filtered per group (fil)
#' @param cexx size of the names in the up part
#' @param cexy size of the names in the left part
#' @return a pdf file with the correlation plot
#' @export
#'
fig.cor<-function (corr, families = Families, variables = Variables, save = "",
                    displ.var = "all",cexx=0.5,cexy=0.5)
{
  require(corrplot)
  # windows()
  par(xpd = T)
  variables <- clean_names(variables)
  liste = variables
  liste2 = families
  listLine <- c(which(!duplicated(liste2)), ncol(corr) + 1)
  corrplot(as.matrix(corr), method = "color", addgrid.col = NA,
           tl.cex = 0.4, mar = rep(1, 4), tl.srt = 45, tl.col = 0)
  #Text up
  text(x = listLine[-length(listLine)] - 1 + diff(listLine)/2,
       ncol(corr) + 1, liste2[listLine[-length(listLine)]],
       srt = 90, cex = cexx, adj = 0)
  #Text left
  if (displ.var == "all") {
    text(y = (ncol(corr)+1):2 -0.85, x = 0, liste, cex = cexy,adj = 1)
  }
  else if (displ.var == "filter") {
    text(y = ncol(corr) - (listLine[-length(listLine)] -
                             2 + diff(listLine)/2), x = 0, liste[listLine[-length(listLine)]],
         cex = cexx, adj = 1)
  }
  #Squares
  for (i in 2:length(listLine)) {
    rect(listLine[i - 1] - 0.5, ncol(corr) - listLine[i - 1] + 1.5, listLine[i] - 0.5, ncol(corr) - listLine[i] +
           1.5, lwd = 1.5)
    lines(rep(listLine[i - 1] - 0.5, 2), c(ncol(corr) - listLine[i - 1] + 0.5, ncol(corr) + 0.5), lty = 1.5)
  }
  rect(0.5, ncol(corr) + 0.5, ncol(corr) + 0.5, 0.5)
  if(save!="")
  {
    dev.copy(pdf, paste(save, ".pdf", sep = ""))
    dev.off()
    dev.off()
  }

}


#' Detect the variables that has only a unique value. Sometimes it creates problems.
#'
#' @param x dataframe
#' @return a vector with the variables that unique
#' @export
#'
which.unique<-function(x)
{
  unico<-NULL
  g<-0
  sapply(x,function(h)
  {

    g<<-g+1
    # print(g)
    t1<-table(as.character(x[,g]))

    # only one value
    if(length(t1)==1)
    {
      # print(2)
      new<-names(x)[g]
      unico<<-c(unico,new)
    }

    # all na are included in
    if(dim(x)[1]==sum(is.na(x[,g])))
    {
      # print(2)
      new<-names(x)[g]
      unico<<-c(unico,new)
    }

  })
  return(unico)
}
