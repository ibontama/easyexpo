#' Comparing by boxplots all the groups
#'
#' @param db dataframe
#' @param izena name of pdf
#' @return pdf with boxplots
#' @export

boxplot3x3<-function(db,izena)
{
  head(db)
  lendb<-dim(db)[2]
  pdf(paste(izena,"_Boxplot_3x3.pdf",sep=""))
  par(mfrow=c(3,3))
  sapply(6:lendb,function(f)
  {
    print(paste(f," -> ",names(db)[f],sep=""))
    type<-fac[which(fac[,2]==names(db)[f]),3]

    # if()
    if(type=="Factor")
    {
      boxplot(db[,3]~db[,f],main=paste(names(db)[3],"->",names(db)[f],sep=""))

    } else if (type=="Numeric")
    {
      # plot(db[,3],db[,f],xlab=names(db)[3],ylab=names(db)[f])
      boxplot(db[,3]~as.factor(db[,f]),main=paste(names(db)[3],"->",names(db)[f],sep=""))
    }
  })
  par(mfrow=c(1,1))
  dev.off()
}
