#' Calculate NA % for each variable and each group
#'
#' @param dataframe dataframe
#' @param variable the variable to group
#' @return Table with %
#' @export
#'

na_table <- function(dataframe, variable = "cohort", plot = FALSE) {
    # Detecting NAs
    dataframe <- data.frame(dataframe[, variable], dataframe[, sapply(dataframe, is.numeric)])
    names(dataframe)[1] <- variable
    coh.un <- unique(dataframe[, variable])

    len.coh <- length(coh.un)
    len.var <- dim(dataframe)[2]
    df.na <- data.frame(matrix(888, len.var, len.coh))
    rownames(df.na) <- names(dataframe)
    names(df.na) <- coh.un
    i <- 0
    j <- 0

    df.na[1, ] <- 0
    # Cities
    for (i in 1:len.coh) {
        # cat(i)
        # cat("\n")
        # Variables
        for (j in 2:len.var) {
            # ifelse(is.numeric(dataframe[,j]),na.pos<-7,na.pos<-3)
            # cat(j)
            # cat(names(dataframe)[j])
            # cat("\n")
            c2 <- dataframe[dataframe[, variable] == coh.un[i], j]
            df.na[j, i] <- ifelse(any(is.na(c2)), round(sum(is.na(dataframe[dataframe[, variable] == coh.un[i], j])) * 100/dim(dataframe[dataframe[,
                variable] == coh.un[i], ])[1], 2), 0)
        }
    }

    # Errezagoa pertsonalizatzeko
    df.na <- df.na[, coh.un]
    mm <- as.matrix(df.na, ncol = 4)

    if(plot)
    {
      par(mar = c(1, 8, 3.5, 1))
      plotrix::color2D.matplot(mm, show.values = TRUE, axes = FALSE, xlab = "", ylab = "", vcex = 0.5, vcol = "black", extremes = c("white",
                                                                                                                                    "red"), na.color = "grey")
      axis(3, at = seq_len(ncol(mm)) - 0.5, labels = colnames(mm), tick = FALSE, cex.axis = 0.7)
      axis(2, at = seq_len(nrow(mm)) - 0.5, labels = rev(rownames(mm)), tick = FALSE, las = 1, cex.axis = 0.7)
    }


    return(mm)
}


#' Calculate NA % for each variable and plot
#'
#' @param data dataframe
#' @param biggertahn value of the percentage
#' @return Plot
#' @export
#'
missing_plot <- function(data,biggerthan = NULL ) {
    mis <- sort(sapply(data, function(x) sum(is.na(x))/length(x) * 100), decreasing = TRUE)
    mis <- as.data.frame(mis)
    mis$names <- rownames(mis)
    mis$kw <- reorder(mis$names, mis$mis)
    mis2 <- mis[,c("names","mis")]
    rownames(mis2)<-c()
    names(mis2)<-c("Variable","MissingPercentage")

    if(is.null(biggerthan))
    {
      g<-ggplot(mis, aes(x =  kw, fill = "1")) + geom_bar(aes(weight = mis)) + coord_flip() + guides(fill = FALSE) + ylab("% of missings") +
        xlab("Variables")
    } else {
      g<-ggplot(mis[mis$mis>biggerthan,], aes(x =  kw, fill = "1")) + geom_bar(aes(weight = mis)) + coord_flip() + guides(fill = FALSE) + ylab("% of missings") +
        xlab("Variables")
    }

    return(list(mis2,g))
}

