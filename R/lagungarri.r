#' Errez ikusteko askotan erabiltzen ditudan gauzak
#'
#' @param nothing nothing
#' @return a list of functions that i use
#' @export

txuleta <- function() {
    
    print("#Function list")
    print("ls('package:easyexpo')")
    print("lsf.str('package:easyexpo')")
    
    print(" ")
    
    print("#Diving plots")
    print("par(mfrow=c(2,2))")
    
    
}

#' Export functions to txt file
#'
#' @param name of the file
#' @return a file with the list
#' @export
#'
txuleta_csv <- function(name) {
    sink(name)
    txuleta()
    sink()
}

#' Convert a NA to zero in a vector
#'
#' @param vector the vector with the data
#' @return a vector with the zeros
#' @export
#'
na2zero <- function(vector) {
    vector[is.na(vector)] <- 0
}

#' Convert numeric-character to numeric
#'
#' @param vector the vector with the data of numeric in character format
#' @return a numeric vector
#' @export
#'
numchar2num <- function(vector) {
    v <- vector[!is.na(vector)]
    v <- as.numeric(v)
    if (sum(is.na(v)) > 0) {
        vector
    } else {
        as.numeric(vector)
    }
}
