#' Create tables in excel and pdf for each of the variable
#'
#' @param dataframe Data frame with all the variables and the group variable
#' @param pdf.name Name of the pdf
#' @param group.var With variable want to use to group the data
#' @return PDF with boxplots
#' @export

descrip_boxplot <- function(dataframe, pdf.name, group.var) {
    dataframe2 <- dataframe[, sapply(dataframe, is.numeric)]
    var1 <- names(dataframe2)

    lista <- list()
    for (i in var1) {
        lista[[i]] <- ggplot(dataframe, aes_string(x = group.var, y = i)) + geom_boxplot() + coord_flip() + labs(x = "",
            y = i) + theme(legend.position = "none", panel.background = element_rect(fill = "white", colour = "black")) +
            theme_bw()
    }

    if(pdf.name!="")
    {
      pdf(pdf.name)
      lapply(lista, print)
      dev.off()
    }
    # else {
    #   lapply(lista, print)
    # }

}


#' Create tables in excel and pdf for each of the variable
#'
#' @param dataframe Data frame with all the variables and the group variable
#' @param pdf.name Name of the pdf
#' @param group.var With variable want to use to group the data
#' @param folder.name name of the folder. In the case of empty the table will appear in the screen
#' @return PDF and Excel with tables by group
#' @export

descrip_table <- function(dataframe, group.var,folder.name="Descriptive_tables") {
    dataframe2 <- dataframe[, sapply(dataframe, is.numeric)]
    var1 <- names(dataframe2)
    day <- day_hour("day")

    if(folder.name!="")
    {
      ifelse(!dir.exists(paste0(folder.name,"/")), dir.create(paste0(folder.name,"/")), FALSE)
    }


    h = 0
    for (i in var1) {
        h = h + 1
        p <- NULL
        df.2 <- data.frame(dataframe[, group.var], dataframe[, var1[h]])
        names(df.2) <- c("Group", "Variable")
        pp <- easyexpo::nire_stats(df.2, group.var2 = "Group")


        if(folder.name!="")
        {
          xlsx::write.xlsx(pp, paste0(folder.name,"/", day, "_Table1-", i, ".xlsx", sep = ""))
          p <- gridExtra::tableGrob(pp)
          ggsave(filename = paste0(folder.name,"/", day, "_Table1-", i, ".pdf", sep = ""), plot = grid.arrange(p), scale = 1,
                 width = 28, height = 20, units = "cm")
        } else {

          p <- gridExtra::tableGrob(pp)
          grid.arrange(p)
        }


    }
    if(folder.name!="")
    {
      print(paste0("All tables saved in ",folder.name," folder"))
    }
}


#' Create basic statistics from a table and the group variable
#'
#' @param dataframe Data frame with all the variables and the group variable
#' @param group.var With variable want to use to group the data
#' @return Table with statistics
#' @export
nire_stats <- function(y, numvar, group.var2) {
    y2 <- y[,numvar]
    coh <- unique(y[, group.var2])
    lencoh <- length(coh)
    ans = NULL
    for (i in 1:lencoh) {
        f <- y2[y[, group.var2] == coh[i]]
        X.length = length(f)
        ff = f[!is.na(f)]
        f <- f[!is.na(f)]
        X.na = X.length - length(ff)

        len.0 <- length(f[f < 0])

        z = c(X.length, X.na, len.0, min(f), as.numeric(quantile(f, prob = 0.25, na.rm = TRUE)), median(f), mean(f), as.numeric(quantile(f,
            prob = 0.75, na.rm = TRUE)), max(f), sum(f))

        znames = c("nobs", "NAs", "Under0", "Minimum", "1. Quartile", "Median", "Mean", "3. Quartile", "Maximum", "sum")
        result = matrix(z, ncol = 1)
        row.names(result) = znames
        ans = cbind(ans, result)
    }
    colnames(ans) = coh
    df <- data.frame(round(ans, digits = 2))
    return(df)
}
