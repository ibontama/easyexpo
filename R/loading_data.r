#' Load any kind of data
#'
#' @param File The name of the file with the path
#' @param header If they have headers
#' @param sep The sign to separate the fileds, default comma (comma, semicolon, tab)
#' @param quote None, single or double quote, default doble quote ("")
#' @return Loaded data frame
#' @export

load_data <- function(File="file.txt",header=TRUE,sep=",",quote="")
  {

      if (is.null(File))
      {
            print("Please include the name of the file")
            return(NULL)
      } else
      {
              form<-which.format(File)
              form <- tolower(form)
              if(form=="csv")
              {
                read.csv(File, header = header,sep = sep, quote = quote)
              } else if(form=="json")
              {

                json_file <- File
                json_data <- jsonlite::fromJSON(json_file)
                print(json_data)

              } else if(form=="xml")
              {
                books <- File
                ldply(xmlToList(books), function(x) { data.frame(x[!names(x)=="author"]) } )
              } else if(form=="xls")
              {

                read.xls (File, sheet = 1, header = TRUE)
              } else if(form=="xlsx")
              {
                read.xls (File, sheet = 1, header = TRUE)
              } else if(form=="rdata")
              {
                fitx <- load (File)
                get(fitx)

              }

      } # else

}


#' Detect the format of the file
#'
#' @param pathfile The name of the file with the path
#' @return Loaded data frame
#' @export
#
which.format<-function(pathfile)
{
  #pathfile<-"adsfa/af.as/safa.csv"
  lenpath<-nchar(pathfile)
  point<-gregexpr('\\.', pathfile)
  point.pos<-point[[1]][length(point[[1]])]
  format<-substr(pathfile,point.pos+1,lenpath)
  return(format)
}
