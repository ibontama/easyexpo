#' Obtain the date or date+hour of the moment
#'
#' @param time1 day, hour or day_hour
#' @return Day, hour or both
#' @export

day_hour <- function(time1) {
    if (time1 == "day") {
        day <- paste(strsplit(as.character(substring(Sys.time(), 1, 10)), "-")[[1]], collapse = "")
        fetxia <- day

    } else if (time1 == "hour") {
        hour <- paste0(substring(Sys.time(), 12, 13), "h", substring(Sys.time(), 15, 16), "m", substring(Sys.time(), 18,
            19), "s")
        fetxia <- hour

    } else if (time1 == "day_hour") {
        day <- paste(strsplit(as.character(substring(Sys.time(), 1, 10)), "-")[[1]], collapse = "")
        hour <- paste0(substring(Sys.time(), 12, 13), "h", substring(Sys.time(), 15, 16), "m", substring(Sys.time(), 18,
            19), "s")
        fetxia <- paste0(day, "_", hour)

    } else {
        print("Choose the right time, please")
    }


    return(fetxia)
}


#' summary of clases
#'
#' @param dataframe with columns
#' @param p if we want a plot
#' @param tb if we want a table
#' @return a summary or a plot
#' @export
#'

s_classes <- function(dataframe, p = FALSE, tb = FALSE) {
    su <- table(sapply(dataframe, class))
    print(su)
    if (p == TRUE) {
        df.freq.cohort <- as.data.frame(table(sapply(dataframe, class)))
        names(df.freq.cohort) <- c("Names", "Freq")

        # Barplot
        ggplot2::ggplot(df.freq.cohort, aes(x = Names, y = Freq, fill = Names)) + geom_bar(stat = "identity") + coord_flip()
    }
    if (tb == TRUE) {
        ncol <- length(su)
        nrow <- as.numeric(sort(su, decreasing = TRUE)[1])
        mm <- data.frame(matrix("-", nrow, ncol))

        for (n in 1:ncol) {
            gehitu <- names(dataframe[, sapply(dataframe, function(x) class(x) == names(su)[n])])
            if (length(gehitu) < nrow) {
                dif <- 170 - length(gehitu)
                gehitu <- c(gehitu, rep("-", dif))
            }
            mm[, n] <- gehitu
        }
        names(mm) <- names(su)
    }


}

#' Obtain the height of the range of a number of vectors
#'
#' @param dd vector with numbers
#' @return I don't know exactly
#' @export

hei.range <- function(dd) {
    rg <- range(dd, na.rm = TRUE)
    rg2 <- round(rg[2] - rg[1], 2)
    return(rg2)
}


#' Transform from matrix to 3 columns data frame
#'
#' @param myMatrix
#' @return dataframe with 3 columns
#' @export
#'
Matriz2colums<-function(myMatrix=x)
{
  lerro<-dim(myMatrix)[1]
  zutabe<-dim(myMatrix)[2]
  hiru<-c("a","a",1)

  for(i in 1:lerro)
  {
    for (j in 1:zutabe)
    {

      hiru<-c(hiru,rownames(myMatrix)[i],colnames(myMatrix)[j],myMatrix[i,j])
    }
  }
  hiru<-hiru[-c(1:3)]
  mul<-zutabe*lerro
  lau<-data.frame(matrix(hiru,nrow = mul,byrow = TRUE))
  names(lau)<-c("Nondik","Nora","Zenbat")
  dataframe<-lau
  return(dataframe)
}


#' Last character
#'
#' @param word string
#' @return last character
#' @export
#'
last_character<-function(word = "kaixo")
{
  len_w <- nchar(word)
  last <- substr(word,len_w,len_w)
  return(last)
}

#' Giving a vector filter with the criteria of the last character
#'
#' @param vector string
#' @param last last character
#' @return those who complete criteria
#' @export
#'
filter_with_last_character<-function(vector = c("_1_","_glm"), last = "_")
{
  len_v <- length(vector)
  yes <- NA
  for(j in 1:len_v)
  {
    if(last_character(vector[j]) == last) yes <- c(yes,vector[j])

  }

  return(yes)
}



#' last_character
#'
#' @param word string
#' @return the last character
#' @export
#'
last_character<-function(word = "kaixo")
{
  len_w <- nchar(word)
  last <- substr(word,len_w,len_w)
  return(last)
}

#' filter_with_last_character
#'
#' @param vector string
#' @param last string
#' @return the last character
#' @export
#'
filter_with_last_character<-function(vector = c("_1_","_glm"), last = "_")
{
  len_v <- length(vector)
  yes <- NA
  for(j in 1:len_v)
  {
    if(last_character(vector[j]) == last) yes <- c(yes,vector[j])

  }
  yes <-yes[-1]
  return(yes)
}
