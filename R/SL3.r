#' General function
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of libraries to use
#' @param outcomes a vector of the outcomes
#' @return each of the empty vectors h
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

general_function <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                             confounders = c("sex"),
                             outcomes = c("Y1"), delta=c(0,1), dr = seq(0,1,0.1))

{
  ptm1=proc.time()
  # preparing variables

  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }

  len_exp <- length(exposures)
  len_cof <- length(confounders)
  var_tot <- c(exposures,confounders)
  len_tot <- len_exp + len_cof
  len_out <- length(outcomes)

  mid_data <- dataset[,c(exposures,confounders)]

  N <- dim(dataset)[1]

  len_delta <- length(delta)
  newData <-rep(NA, len_tot)
  len_exp2 <- len_exp*len_delta

  ######################################################################################################
  # We create a copy of database, so then we can change values

  for(bat in 1:len_exp2)
  {
    newData <- rbind(newData,mid_data)
  }
  newData <- newData[-1,]

  # put values of the delta (0 and 1 for example)

  h <- 0
  n <- 1
  laburpenCC <- NA
  for(bat in 1:len_exp)
  {
    for(bi in 1:len_delta)
    {
      h <- h + 1
      n2 <- n + N
      newData[n:(n2-1),bat]<-delta[bi]
      laburpenCC <- c(laburpenCC,"CC",exposures[bat],delta[bi],n,(n2-1))
      n <- n2
    }
  }
  laburpenCC <- laburpenCC[-1]
  laburpenCC2 <- (data.frame(matrix(laburpenCC,ncol = 5,byrow = TRUE)))

  # matrix(laburpenCC,ncol = 5)
  # names(newData) <- var_tot

  zenbatcc <- n2 - 1

  ######################################################################################################
  # Creating new data for secuences

  sekuentzia <- dr
  len_sek <- length(sekuentzia)

  #Models#
  #PCB#

  quar <- paste0("newData2")
  assign(quar,data.frame(matrix(NA,1,len_tot)))
  quary <- "newData_sek"
  assign(quary,data.frame(matrix(NA,1,len_tot)))
  assign("tartey",get(quary))
  names(tartey) <- names(dataset[,var_tot])
  assign(quary,tartey)

  len_sek_exp <- len_sek * len_exp

  for(r in 1:len_sek)
  {
    # print(r)
    tarteko <- get("newData2")
    names(tarteko) <- names(dataset[,var_tot])
    assign(quar, rbind(tarteko,dataset[,var_tot]))
  }

  assign(quar, get(quar)[-1,])

  ######################################################################################################
  # calculo de percentiles
  per_value <- NA


  h <- 0
  for(s in 1:len_exp)
  {
    # print(exposures[s])
    for(f in sekuentzia)
    {
      h <- h + 1
      # print(h)
      valor <- dataset[,exposures[s]]
      per_value <- c(per_value,as.numeric(quantile(valor,f)))

    }

  }


  per_value <- per_value[-1]

  # vec sortu
  h <- 0
  n <- 1
  vec <- n
  for(bat in 1:len_sek)
  {
    n2 <- n + N
    #300
    vec <- c(vec, (n2-1))
    # newData[n:(n2-1),bat]<-1
    # 301
    n <- n2
    vec <- c(vec, (n2))
    # n2 <- n + N
    # #
    # vec <- c(vec, (n2-1))
    # # newData[n:(n2-1),bat]<-0
    # n <- n2
    # vec <- c(vec, n)
  }
  vec <- vec[-length(vec)]

  z2gehi <- vec[length(vec)]

  ######################################################################################################
  # asignacion de percentiles
  laburpenDR <- NA
  h <- 0

  zgehi <- 0
  for(s in 1:len_exp)
  {
    # s <- 1
    quar1 <- paste0("newData2_",exposures[s])
    tarteko <- get("newData2")
    names(tarteko) <- names(dataset[,var_tot])
    assign(quar1,tarteko)

    g <- 0
    h <- 0

    for(f in 1:len_sek)
    {
      # print(f)
      g <- g + 1
      h1 <- h + 1
      h2 <- h1 + 1

      tarteko2 <- get(quar1)
      tarteko2[vec[h1]:vec[h2],exposures[s]]<-per_value[g]
      # laburpenICE <- c(laburpenICE,paste0("Indiv_",f),exposures[s],f,vec[h1],vec[h2])
      laburpenDR <- c(laburpenDR,paste0("DR_",f),exposures[s],f,vec[h1]+zgehi,vec[h2]+zgehi)
      assign(quar1, tarteko2)

      h <- h2

    }

    zgehi <- zgehi + z2gehi

    assign(quary, rbind(get(quary),get(quar1)))
  }

  laburpenDR <- laburpenDR[-1]
  laburpenDR2 <- data.frame(matrix(laburpenDR,ncol=5,byrow = TRUE))

  new_data3 <- get(quary)
  new_data3 <- new_data3[-1,]

  zenbatdr<- n2 - 1
  # zenbatite <- n2
  # ez dakit individualak falta diren

  ######################################################################################################
  # Creating new data for interaction

  laburpenITE <- NA

  quar <- paste0("newData_it")
  assign(quar,data.frame(matrix(NA,1,len_tot)))
  # quary <- "newData_sek"
  # assign(quary,data.frame(matrix(NA,1,6)))
  # assign("tartey",get(quary))
  names(newData_it) <- names(dataset[,var_tot])
  # assign(quary,tartey)

  z <- len_tot
  n_inter <- z*(z-1)/2
  len_4_int <- 4 * n_inter

  for(r in 1:len_4_int)
  {
    # print(r)
    tarteko <- get("newData_it")
    names(tarteko) <- names(dataset[,var_tot])
    assign(quar, rbind(tarteko,dataset[,var_tot]))
  }

  assign(quar, get(quar)[-1,])

  ################################################## interactions values

  ec <- 1
  h <- 0
  n <- 1

  for(ex in 1:len_tot)
  {
    # ex <- 1
    # print(ex)
    ec <- ec + 1
    if(len_tot==ex) break
    for(ex2 in ec:len_tot)
    {
      # ex2 <- 2
      # print(ex2)
      # print(paste0(var_tot[ex],"-",var_tot[ex2]))

      # poner 0 y 1
      h <- h + 1
      n2 <- n + N
      newData_it[n:(n2-1),ex]<-delta[1]
      newData_it[n:(n2-1),ex2]<-delta[1]
      laburpenITE <- c(laburpenITE,paste0("ITE_",var_tot[ex]),var_tot[ex2],00,n,(n2-1))
      n <- n2
      n2 <- n + N
      h <- h + 1
      newData_it[n:(n2-1),ex]<-delta[1]
      newData_it[n:(n2-1),ex2]<-delta[2]
      laburpenITE <- c(laburpenITE,paste0("ITE_",var_tot[ex]),var_tot[ex2],01,n,(n2-1))
      n <- n2
      n2 <- n + N
      h <- h + 1
      newData_it[n:(n2-1),ex]<-delta[2]
      newData_it[n:(n2-1),ex2]<-delta[1]
      laburpenITE <- c(laburpenITE,paste0("ITE_",var_tot[ex]),var_tot[ex2],10,n,(n2-1))
      n <- n2
      n2 <- n + N
      h <- h + 1
      newData_it[n:(n2-1),ex]<-delta[2]
      newData_it[n:(n2-1),ex2]<-delta[2]
      laburpenITE <- c(laburpenITE,paste0("ITE_",var_tot[ex]),var_tot[ex2],11,n,(n2-1))
      n <- n2

    }
  }

  laburpenITE <- laburpenITE[-1]
  laburpenITE2 <- data.frame(matrix(laburpenITE,len_4_int,5,byrow = TRUE))

  zenbatiter <- n2 - 1

  # Merging all the data

  new_tot <- rbind(newData,new_data3,newData_it)


  names(laburpenCC2) <- names(laburpenDR2) <- names(laburpenITE2) <- c("Group","Case","SubGroup","From","To")

  laburtot <- rbind(laburpenCC2,laburpenDR2,laburpenITE2)

  laburtot$From <- as.numeric(as.character(laburtot$From))
  laburtot$To <- as.numeric(as.character(laburtot$To))

  lastlab <- dim(laburtot)[1]
  len3 <- len_delta*len_exp + 1
  len4 <- len3 + len_sek_exp
  len5 <- len4 + len3
  laburtot$From[c((len3):lastlab)] <- laburtot$From[(len3):lastlab]+zenbatcc
  laburtot$To[c((len3):lastlab)] <- laburtot$To[(len3):lastlab]+zenbatcc
  laburtot$From[(len4:lastlab)] <- laburtot$From[len4:lastlab] + (zenbatdr * len_exp)
  laburtot$To[(len4:lastlab)] <- laburtot$To[len4:lastlab] + (zenbatdr * len_exp)
  # laburtot$From[(zenbatite+1):lastlab] <- laburtot$From[(zenbatite+1):lastlab]+zenbatite
  # laburtot$From[(zenbatiter+1):lastlab] <- laburtot$From[(zenbatiter+1):lastlab]+zenbatiter

  return(list(new_tot,laburtot))
}




#' Simulations
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of libraries to use
#' @param outcomes a vector of the outcomes
#' @param num.sim number of simulations
#' @return each of the empty vectors
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

run_simulations <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                            confounders = c("sex"), libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"),
                            outcomes = c("Y1"), num.sim = 50, delta=c(0,1), dr = seq(0,1,0.1), newData = gen,
                            show_times =FALSE, save_time =FALSE)

{
  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }


  len_libraries <- length(libraries)
  if(len_libraries==0)
  {
    simple_libr <- c("")
  } else {
    simple_libr <- gsub("SL.","",libraries)
    simple_libr <- substr(gsub("\\.","",simple_libr),1,5)
    simple_libr <- c("",simple_libr)
  }
  len_libraries <- length(libraries)
  len_exp <- length(exposures)
  len_cof <- length(confounders)
  var_tot <- c(exposures,confounders)
  len_tot <- len_exp + len_cof
  len_out <- length(outcomes)

  mid_data <- dataset[,c(exposures,confounders)]
  d=dataset
  len_new <- dim(newData)[1]

  big_matrix <- data.frame(matrix(NA,len_new,num.sim))

  risk <- data.frame(libraries,matrix(NA,len_libraries,num.sim))
  coef <- data.frame(libraries,matrix(NA,len_libraries,num.sim))

  # Per each simulation all the process
  # num.sim = 2
  if(save_time==TRUE) time3 <- NA
  t1 <- Sys.time()

  for(i in 1:num.sim)
  {
    print(paste0("Simulation: ",i))
    # i <- 1
    # print(paste0("i:",i))


    # To take different values for each simulation
    idx=sample(1:N,N,replace=T)
    dataset=d[idx,]

    ####G-computation using superlearner
    t1 <- Sys.time()
    for(outc in 1:len_out)
    {

      nam <- paste0("SL.fit_",outcomes[outc])
      assign(nam, SuperLearner(Y=dataset[,outcomes[outc]], X=dataset[,var_tot], SL.library=libraries,
                               newX=newData[,var_tot], verbose=FALSE,
                               cvControl=list(V=3),family = family, method =method))
    }

    elkar2 <- get(nam)
    names(elkar2)

    risk[,i+1] <- elkar2$cvRisk
    coef[,i+1] <- elkar2$coef

    # if(show_times==TRUE)
    # {
    #   t2 <- Sys.time()
    #   (t2-t1)
    # }



    #Predictions#
    h <- 0
    n <- 1

    # creamos vectores para posiciones de los registros
    len_new <- dim(newData)[1]

    vec <- n
    for(bat in 1:len_new)
    {
      n2 <- n + N
      #300
      vec <- c(vec, (n2-1))
      # newData[n:(n2-1),bat]<-1
      # 301
      n <- n2
      # vec <- c(vec, (n2))
      # n2 <- n + N
      #
      # vec <- c(vec, (n2-1))
      # newData[n:(n2-1),bat]<-0
      n <- n2
      vec <- c(vec, n)
    }
    vec <- vec[-length(vec)]

    # traemos ate
    # ate <- names(lst)
    # outco <- names(lst2)
    # outco <- grep("SL.fit",ls(),value=TRUE)

    h <- 0
    g <- 0
    m <- 0

    # len_ate <- length(ate)



    ################################################################
    # Calculos para cada modelo, metodologia y outcomes
    ################################################################


    m <- 0

    # for(outt in outco)
    # {
    # outt <- outco[1]
    # print(paste0("outt:",outt))
    modelo <- get(nam)
    vecy <- as.numeric(modelo$SL.predict)
    # print(class(modelo$SL.predict))
    # print(class(vecy))
    # print(i)
    big_matrix[,i] <- modelo$SL.predict[,1]
    # head(big_matrix)
    # big_matrix[,i] <- (modelo$SL.predict)
    # }

    if(show_times==TRUE)
    {
      t2 <- Sys.time()
      # print(paste0("Simulation ", i))
      print(t2-t1)
      if(save_time==TRUE) time3 <- c(time3,t2-t1)
    }


  } # for(i)


  fecha <- format(Sys.time(), "%Y%m%d%X")
  fecha <- gsub(":","",fecha)
  # save.image(paste0(fecha,"_ATEs_new4.RData"))
  # save(big_matrix,file=paste0(fecha,"_big_matrix.RData"))
  return(list(big_matrix,risk,coef,time_proc=time3))

}


#' Simulations
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of libraries to use
#' @param outcomes a vector of the outcomes
#' @param num.sim number of simulations
#' @return each of the empty vectors
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

run_simulations_improved <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                            confounders = c("sex"), libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"),
                            outcomes = c("Y1"), num.sim = 50, delta=c(0,1), dr = seq(0,1,0.1), newData = gen,
                            show_times =FALSE, save_time =FALSE,family = "gaussian", method = "method.NNLS")

{
  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }


  len_libraries <- length(libraries)
  if(len_libraries==0)
  {
    simple_libr <- c("")
  } else {
    simple_libr <- gsub("SL.","",libraries)
    simple_libr <- substr(gsub("\\.","",simple_libr),1,5)
    simple_libr <- c("",simple_libr)
  }
  len_libraries <- length(libraries)
  len_exp <- length(exposures)
  len_cof <- length(confounders)
  var_tot <- c(exposures,confounders)
  len_tot <- len_exp + len_cof
  len_out <- length(outcomes)

  mid_data <- dataset[,c(exposures,confounders)]
  d=dataset
  len_new <- dim(newData)[1]

  big_matrix <- data.frame(matrix(NA,len_new,num.sim))

  risk <- data.frame(libraries,matrix(NA,len_libraries,num.sim))
  coef <- data.frame(libraries,matrix(NA,len_libraries,num.sim))

  # Per each simulation all the process
  # num.sim = 2
  if(save_time==TRUE) time3 <- NA
  t1 <- Sys.time()

  for(i in 1:num.sim)
  {
    print(paste0("Simulation: ",i))
    idx=sample(1:N,N,replace=T)
    dataset=d[idx,]

    ####G-computation using superlearner
    t1 <- Sys.time()

    modelo <- SuperLearner(Y=dataset[,outcomes], X=dataset[,var_tot], SL.library=libraries,
                           family = family, method =method, newX=newData[,var_tot], verbose=FALSE)

    risk[,i+1] <- modelo$cvRisk
    coef[,i+1] <- modelo$coef
    big_matrix[,i] <- modelo$SL.predict[,1]

    if(show_times==TRUE)
    {
      t2 <- Sys.time()
      print(t2-t1)
      if(save_time==TRUE) time3 <- c(time3,t2-t1)
    }


  } # for(i)


  fecha <- format(Sys.time(), "%Y%m%d%X")
  fecha <- gsub(":","",fecha)
  return(list(big_matrix,risk,coef,time_proc=time3))

}

#' Simulations
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of libraries to use
#' @param outcomes a vector of the outcomes
#' @param num.sim number of simulations
#' @return each of the empty vectors
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

run_simulations_improved_paralel <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                                     confounders = c("sex"), libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"),
                                     outcomes = c("Y1"), num.sim = 50, delta=c(0,1), dr = seq(0,1,0.1), newData = gen,
                                     show_times =FALSE, save_time =FALSE, verbose=FALSE,family = "gaussian", method = "method.NNLS")

{
  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }


  len_libraries <- length(libraries)
  if(len_libraries==0)
  {
    simple_libr <- c("")
  } else {
    simple_libr <- gsub("SL.","",libraries)
    simple_libr <- substr(gsub("\\.","",simple_libr),1,5)
    simple_libr <- c("",simple_libr)
  }
  len_libraries <- length(libraries)
  len_exp <- length(exposures)
  len_cof <- length(confounders)
  var_tot <- c(exposures,confounders)
  len_tot <- len_exp + len_cof
  len_out <- length(outcomes)
  # var_tot <- c(exposures,confounders)

  mid_data <- dataset[,c(exposures,confounders)]
  d=dataset
  N <- dim(d)[1]
  len_new <- dim(newData)[1]

  big_matrix <- data.frame(matrix(NA,len_new,num.sim))

  risk <- data.frame(libraries,matrix(NA,len_libraries,num.sim))
  coef <- data.frame(libraries,matrix(NA,len_libraries,num.sim))

  # Per each simulation all the process
  # num.sim = 2
  if(save_time==TRUE) time3 <- NA
  t1 <- Sys.time()
  # N <- dim(d)[1]
  # Paralilazing
          cores=detectCores()
          cl <- makeCluster(cores[1]-1) #not to overload your computer
          registerDoParallel(cl)


          # t1 <- Sys.time()
          registerDoSEQ()
          finalMatrix <- foreach(i=1:num.sim, .combine=cbind, .packages = "SuperLearner") %dopar% {
            if(verbose==TRUE)
            {
              print(paste0("Run number ",i, "/",num.sim))
            }


            idx = sample(1:N, N, replace = T)
            dataset = d[idx, ]

            modelo <- SuperLearner::SuperLearner(Y = dataset[, outcomes],
                                                 X = dataset[, var_tot], SL.library = libraries,
                                                 newX = newData[, var_tot], verbose = FALSE,
                                                 family = family, method =method)
            risk[,i+1] <- modelo$cvRisk
            coef[,i+1] <- modelo$coef
            big_matrix[,i] <- modelo$SL.predict[,1]
            gc()

          }


          #stop cluster
          stopCluster(cl)


    if(show_times==TRUE)
    {
      t2 <- Sys.time()
      print(t2-t1)
      if(save_time==TRUE) time3 <- c(time3,t2-t1)
    }

  fecha <- format(Sys.time(), "%Y%m%d%X")
  fecha <- gsub(":","",fecha)
  return(list(big_matrix,risk,coef,time_proc=time3))

}

#' ACE
#'
#' @param big result of the simulation
#' @param dataset dataset with all variables
#' @param ic_dis select ic (confidence intervals) or dis (distribution)
#' @return a vector
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"


ace.f <- function(big = simu[[1]], dataset = data.X2, ic_dis = "IC")
{

  N <- dim(dataset)[1]
  sim <- dim(big)[2]
  m2 <- NA
  mean.res <- mean(as.matrix(big[1:N,]))-mean(as.matrix(big[(N+1):(N*2),]))

  for(f in 1:sim)
  {
    # print(f)
    m1 <- mean(as.matrix(big[1:N,f]))-mean(as.matrix(big[(N+1):(N*2),f]))
    m2 <- c(m2,m1)
  }

  m2 <- m2[-1]

  if(ic_dis == "IC")
  {
    # IC people can choose
    sdd <- sd(m2[-1])
    lci <- mean.res-sdd*1.96
    uci <- mean.res+sdd*1.96

  } else if (ic_dis == "dis")
  {
    # distribution
    lci <- quantile(m2,0.025)
    uci <-  quantile(m2,0.975)
  }

  return(list(mean.res,lci,uci))

}

#' ACE group
#'
#' @param big result of the simulation
#' @param dataset dataset with all variables
#' @param ic_dis select ic (confidence intervals) or dis (distribution)
#' @return a vector
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

ace.group <- function(big2 = simu[[1]], dataset = data.X2, ic_dis = "IC", st = summay_table_lines,
                      exposures = c("pcb","pfos","hcb","ppdde","pfoa"), delta = c(0,1))
{

      # exposures = c("pcb","pfos","hcb","ppdde","pfoa")
      # st <- summay_table_lines
      st2 <- as.character(st[,1])

      # un_dr <- unique(grep("DR_",st2,value=TRUE))
      len_exp <- length(exposures)
      len_delta <- length(delta)
      df_ace <- data.frame(matrix(NA,len_exp,4))
      names(df_ace) <- c("Group","Mean","ICa","ICb")

      h <- 0
      for(ex in 1:len_exp)
      {
        # for(g in 1:len_delta)
        # {
        h <- h + 1
        df_ace[h,"Group"] <- paste0(exposures[ex])

        stp <- st[st$Group=="CC" & st$Case==exposures[ex],c(4,5)]
        from <- as.numeric(stp[1,1])
        to <- as.numeric(stp[2,2])
        mdata <- big2[from:to,]

        b <- ace.f (big = mdata, dataset = data.X2, ic_dis = "IC")

        df_ace[h,"Mean"] <- b[[1]]
        df_ace[h,"ICa"] <- b[[2]]
        df_ace[h,"ICb"] <- b[[3]]
        # }
      }
  return(df_ace)
}



#' DR Dosis Response
#'
#' @param big result of the simulation
#' @param dataset dataset with all variables
#' @param dr sequence of values that we want to calculate
#' @return a vector
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"


dr.f <- function(big = simu,dataset = data.X2,dr = seq(0,1,0.1))
{

  N <- dim(dataset)[1]
  sim <- dim(big)[2]
  size <- length(dr)
  m2 <- NA

  sum_dr <- data.frame(matrix(NA,size,3))
  names(sum_dr) <- c("Quantile","Mean","SE")

  pos <- 1
  for(g in 1:size)
  {
    # g <- 1
    newpos <- pos + N -1
    m3 <- as.matrix(big[pos:newpos,])

    for(f in 1:sim)
    {
      # print(f)
      m1 <- mean(as.matrix(big[pos:newpos,f]))
      m2 <- c(m2,m1)
    }
    m2 <- m2[-1]
    # IC people can choose
    se <- sd(m2)

    sum_dr[g,2] <- mean(m3)
    sum_dr[g,3] <- se
    sum_dr[g,1] <- paste0("DR_",dr[g])
    pos <- newpos+1
  }

  return(sum_dr)

}


#' DR group
#'
#' @param big result of the simulation
#' @param dataset dataset with all variables
#' @param ic_dis select ic (confidence intervals) or dis (distribution)
#' @return a vector
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

dr.group <- function(big2 = simu[[1]], dataset = data.X2, ic_dis = "IC", st = summay_table_lines,
                      exposures = c("pcb","pfos","hcb","ppdde","pfoa"),dr = seq(0,1,0.1))
{

      # exposures = c("pcb","pfos","hcb","ppdde","pfoa")
      # st <- summay_table_lines
      st2 <- as.character(st[,1])

      un_dr <- unique(grep("DR_",st2,value=TRUE))
      len_exp <- length(exposures)
      len_dr <- length(un_dr)
      df_ace <- data.frame(matrix(NA,len_exp*len_dr,4))
      names(df_ace) <- c("Group","Mean","ICa","ICb")

      b2 <- data.frame(matrix(NA,1,4))
      names(b2) <- c("Quantile","Mean","SE","Exp")

      h <- 0
      for(ex in 1:len_exp)
      {
        # ex <- 1
        # for(g in 1:len_dr)
        # {
        # g <- 1
        h <- h + 1
        # df_ace[h,"Group"] <- paste0(exposures[ex],"_",un_dr[g])

        # stp <- st[st$Group==un_dr[ex] & st$Case==exposures[ex],c(4,5)]
        stp <- st[st$Case==exposures[ex],c(4,5)]
        ####!!!!
        #kontuz 3 jarritako posizioan, zeren ezberdina izango da delta handiagoa baldin bada
        from <- as.numeric(stp[3,1])
        to <- as.numeric(stp[dim(stp)[1],2])
        mdata <- big2[from:to,]

        b <- dr.f (big = mdata, dataset = data.X2, dr = dr)
        b$Exp <- exposures[ex]
        b2 <- rbind(b2,b)
      }
    b2 <- b2[-1,]
  return(b2)
}

#' ICE
#'
#' @param big result of the simulation
#' @param dataset dataset with all variables
#' @param dr sequence of values that we want to calculate
#' @return a vector
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"


ice.f <- function(big = simu,dataset = data.X2,dr = seq(0,1,0.1))
{

  N <- dim(dataset)[1]
  sim <- dim(big)[2]
  size <- length(dr)
  m2 <- NA

  ice <- data.frame(matrix(NA,N,size))
  # names(ice)[1] <- c("Quantile")

  pos <- (N*2)+1
  for(g in 1:size)
  {
    newpos <- pos + N -1
    m3 <- big[pos:newpos,1]
    names(ice)[g] <- paste0("DR_",dr[g])
    ice[,g]<-m3
    pos <- newpos+1
  }

  return(ice)

}



#' Interaction
#'
#' @param big result of the simulation
#' @param dataset dataset with all variables
#' @param dr sequence of values that we want to calculate
#' @param newData Likily to come from simulation
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @return a vector
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"



interaction_f2 <- function(big = simu,dataset = data.X2,delta=c(0,1),dr = seq(0,1,0.1),newData = gen,
                           exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                           confounders = c("sex"))
{



  N <- dim(dataset)[1]
  sim <- dim(big)[2]
  size <- length(dr)
  m2 <- NA
  len_sek <- length(dr)
  len_cof <- length(confounders)
  len_exp <- length(exposures)
  len_tot <- len_exp + len_cof
  var_tot <- c(exposures,confounders)

  hasera <- (N * 2) + N * len_sek * (len_exp) +1

  big <- big[-c(1:(hasera-1)),]


  h <- 0
  n <- 1

  z <- len_exp+len_cof
  z2 <- (z*(z-1))/2
  azken_taula <- data.frame(matrix(NA,z2,sim+1))
  names(azken_taula)[1] <- c("Interaction")

  #Izenak jartzeko
  ec <- 1
  h <- 1
  for(ex in 1:len_tot)
  {
    ec <- ec + 1
    if(len_tot==ex) break
    for(ex2 in ec:len_tot)
    {
      inter2 <- paste0(var_tot[ex],"-",var_tot[ex2])
      # print(inter2)
      azken_taula[h,1] <- inter2
      h <- h + 1
    }
  }

  # Eman balioa

  ec <- 1
  h3 <- 1
  gehi <- 0

  for(ex in 1:len_tot)
  {
    # ex <- 1
    # print(ex)

    ec <- ec + 1

    if(len_tot==ex) break
    for(ex2 in ec:len_tot)
    {
      # h <- h + 1
      # ex2 <- 2
      # print(ex2)
      inter2 <- paste0(var_tot[ex],"-",var_tot[ex2])
      # print(inter2)
      # rownames(azken_taula)[ec] <- inter2

      for(si in 1:sim)
      {
        # si <- 1
        n <- 1 + gehi

        # poner 0 y 1
        # h <- h + 1
        n2 <- n + N

        a <- big[n:(n2-1),si]

        n <- n2
        n2 <- n + N
        # h <- h + 1

        b <- big[n:(n2-1),si]

        n <- n2
        n2 <- n + N
        # h <- h + 1

        c <- big[n:(n2-1),si]

        n <- n2
        n2 <- n + N
        # h <- h + 1

        d <- big[n:(n2-1),si]

        n <- n2

        azken_taula[h3,si+1] <- mean(a - b - c + d)
      }

      gehi <- gehi + 4 * N
      h3 <- h3  + 1
    }
  }

  # Falta calcular las medias y sd

  azken_taula2 <- data.frame(matrix(NA,z2,3))
  # names(azken_taula2) <- c("Interaction","Mean","SD")
  # Begiratu beste funtzioan
  # library(dplyr)
  # dt[,list(mean=mean(age),sd=sd(age)),by=group]
  m1 <- apply(azken_taula[,-1],1,mean)
  m2 <- apply(azken_taula[,-1],1,sd)

  azken_taula2 <- data.frame(azken_taula$Interaction,m1,m2)
  names(azken_taula2)<- c("Interaction","Mean","SD")

  return(azken_taula2)

}


