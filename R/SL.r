#' Create ATE
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of libraries to use
#' @param outcomes a vector of the outcomes
#' @param num.sim number of simulations
#' @param plotate boolean to decide if we want to plot
#' @return each of the empty vectors
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

ate_calculate <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                          confounders = c("sex"), libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"),
                          outcomes = c("Y1","Y2","Y3","Y4"),
                          num.sim = 3, plotate = FALSE,secuence=c(0,1))

{

  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }


  ptm1=proc.time()
  # preparing variables
  len_libraries <- length(libraries)
  if(len_libraries==0)
  {
    simple_libr <- c("")
  } else {
    simple_libr <- gsub("SL.","",libraries)
    simple_libr <- substr(gsub("\\.","",simple_libr),1,5)
    simple_libr <- c("",simple_libr)
  }
  len_libraries <- length(libraries)
  len_exp <- length(exposures)
  len_cof <- length(confounders)
  var_tot <- c(exposures,confounders)
  len_tot <- len_exp + len_cof
  len_out <- length(outcomes)

  mid_data <- dataset[,c(exposures,confounders)]
  d=dataset

  # Create vectors with empty data
  lst <- list()
  lst2 <- list()

  # per each library
  for(lib in simple_libr)
  {
    # per each outcome
    for(ou in 1:len_out)
    {
      # per each exposure
      for(ex in 1:len_exp)
      {
        nam <- paste0("ATE_",lib,"_",exposures[ex],"_",ou)
        lst[[nam]] <- assign(nam, rep(NA, num.sim))

      }

    }
  }

  # Per each simulation all the process
  N <- dim(dataset)[1]
  for(i in 1:num.sim)
  {
    # i <- 1
    print(paste0("i:",i))


    # To take different values for each simulation
    idx=sample(1:N,N,replace=T)
    dataset=d[idx,]


    n_sek <- length(secuence)
    newData <-rep(NA, len_tot)
    len_exp2 <- len_exp*n_sek

    # We create a copy of database, so then we can change values
    for(bat in 1:len_exp2)
    {
      newData <- rbind(newData,mid_data)
    }
    newData <- newData[-1,]



    # put values of the secuence (0 and 1 for example)

    h <- 0
    n <- 1
    for(bat in 1:len_exp)
    {
      for(bi in 1:n_sek)
      {
        h <- h + 1
        n2 <- n + N
        newData[n:(n2-1),bat]<-secuence[bi]
        n <- n2
      }
    }
    # names(newData) <- var_tot


    ####G-computation using superlearner
    t1 <- Sys.time()
    for(outc in 1:len_out)
    {
      print(paste0("outcome:",outc))
      nam <- paste0("SL.fit_",outcomes[outc])
      lst2[[nam]] <- assign(nam,
                            SuperLearner(Y=dataset[,outcomes[outc]], X=dataset[,var_tot], SL.library=libraries,
                                         family="gaussian",method="method.NNLS", newX=newData[,var_tot], verbose=FALSE))
    }

    t2 <- Sys.time()
    (t2-t1)


    #Predictions#
    h <- 0
    n <- 1

    # creamos vectores para posiciones de los registros
    vec <- n
    for(bat in 1:len_exp2)
    {
      n2 <- n + N
      #300
      vec <- c(vec, (n2-1))
      # newData[n:(n2-1),bat]<-1
      # 301
      n <- n2
      # vec <- c(vec, (n2))
      # n2 <- n + N
      #
      # vec <- c(vec, (n2-1))
      # newData[n:(n2-1),bat]<-0
      n <- n2
      vec <- c(vec, n)
    }
    vec <- vec[-length(vec)]

    # traemos ate
    ate <- names(lst)
    # outco <- names(lst2)
    outco <- grep("SL.fit",ls(),value=TRUE)

    h <- 0
    g <- 0
    m <- 0

    len_ate <- length(ate)

    ################################################################
    # Calculos para cada modelo, metodologia y outcomes
    ################################################################
    print("before lib")
    sel <- 0
    for(lib in simple_libr)
    {
      # lib <- simple_libr[2]
      m <- 0

      print(paste0("lib:",lib))

      if(lib == "") # superlearning 1
      {
        print("SL")
        for(outt in outco)
        {
          # outt <- outco[1]
          print(paste0("outt:",outt))
          m <- m + 1 # m de modelo no de metodo
          exp <- 0
          modelo <- get(outt)

          h <- 0

          for(e in 1:len_exp)
          {
            # e <- 1
            exp <- exp + 1
            print(paste0("ex_",exposures[exp],"_model_",outt,"_SL"))

            # g <- g + 1
            h1 <- h + 1
            h2 <- h1 + 1
            h3 <- h2 + 1
            h4 <- h3 + 1


            gr1 <- grep(m,ate,value=TRUE)
            gr1 <- grep("__",gr1,value=TRUE)
            gr2 <- grep(exposures[exp],gr1,value=TRUE)

            Object = get(gr2)
            print(gr2)
            kalkulua <- mean(modelo$SL.predict[vec[h1]:vec[h2]] - modelo$SL.predict[vec[h3]:vec[h4]])
            lst[[gr2]][i] <- kalkulua
            Object[i] <- kalkulua
            print(lst[[gr2]][i])
            assign(gr2, Object)

            h <- h4
          } # for exp
        } # for outtcome

      } else {  # other libraries
        print("Beste aukera")

        sel <- sel + 1

        for(outt in outco)
        {

          print(paste0("outt:",outt))
          m <- m + 1 # m de modelo no de metodo
          exp <- 0
          modelo <- get(outt)
          h <- 0

          for(e in 1:len_exp)
          {
            exp <- exp + 1
            print(paste0("ex_",exposures[exp],"_model_",outt,"_",lib))

            # g <- g + 1
            h1 <- h + 1
            h2 <- h1 + 1
            h3 <- h2 + 1
            h4 <- h3 + 1


            gr1 <- grep(m,ate,value=TRUE)
            gr2 <- grep(exposures[exp],gr1,value=TRUE)
            gr2 <- grep(lib,gr2,value=TRUE)

            if(lib=="glm")
            {
              gr2 <- gr2[1]
            }
            print(paste0("gr2:",gr2))
            Object = get(gr2)
            # kalkulua <- mean(modelo$SL.predict[vec[h1]:vec[h2]] - modelo$SL.predict[vec[h2]+1:vec[h3]])
            kalkulua <- mean(modelo$library.predict[vec[h1]:vec[h2],sel] - modelo$library.predict[vec[h3]:vec[h4],sel])
            lst[[gr2]][i] <- kalkulua
            Object[i] <- kalkulua
            assign(gr2, Object)
            print(lst[[gr2]][i])
            h <- h4
          } # for exp

        }



      }

    }  # for(lib)
  } # for(i)


  fecha <- format(Sys.time(), "%Y%m%d%X")
  fecha <- gsub(":","",fecha)
  # save.image(paste0(fecha,"_ATEs_new4.RData"))

  #     return(list(lst,lst2,simple_libr,lib))
  # }
  ###########################################################################
  ###    Juntamos resultados
  ##########################################################################

  # load("20171103164358_ATEs_new4.RData")

  ATE <- data.frame(matrix(NA,1,6))


  #############################################
  ###    Loop para juntar
  #############################################
  for(lib in simple_libr)
  {
    # lib <- simple_libr[3]
    m <- 0

    handia <- paste0("ATEs_",lib)
    assign(handia,data.frame(matrix(NA,1,6)))

    # ATE
    # ATEs_ ("",glm)
    # ATEs_1_ ("",glm)
    # ATE__pcb_1


    if(lib == "") # superlearning 1
    {
      for(outt in outco)
      {
        m <- m + 1 # m de modelo no de metodo, osea outtcome

        izena <- paste0("ATEs",m,"_",lib)
        assign(izena,data.frame(matrix(NA,1,5)))
        print(izena)

        # izena <- paste0("ATEs",m,"_",lib)
        # assign(izena,data.frame(matrix(NA,1,5)))


        exp <- 0
        modelo <- get(outt)
        h <- 0

        for(e in 1:len_exp)
        {
          print(paste0("e:",e))
          exp <- exp + 1
          print(paste0("exp_",exposures[exp],"_model_",outt,"_SL"))

          zein <- paste0("ATE__",exposures[exp],"_",m)
          print(zein)
          datos <- as.data.frame(cbind(paste0("Exposure-response ",m),exposures[exp],mean(lst[[zein]]),quantile(lst[[zein]],0.025),quantile(lst[[zein]],0.975)))
          names(datos) <- names(get(izena))
          assign(izena,rbind (get(izena), datos))



        } # for exp

        zein2 <- as.data.frame(get(izena))
        zein2$Algorithm<- "SL"
        names(zein2)[6]<-"X6"
        assign(handia,rbind (get(handia), zein2))

        # assign(paste0("ATEs",m),rbind())

      } # for outtcome

    } else {  # other libraries

      for(outt in outco)
      {
        # outt <- outco[1]
        m <- m + 1 # m de modelo no de metodo

        izena <- paste0("ATEs",m,"_",lib)
        assign(izena,data.frame(matrix(NA,1,5)))
        print(izena)

        exp <- 0
        modelo <- get(outt)
        h <- 0

        for(e in 1:len_exp)
        {
          # e <- 1

          exp <- exp + 1
          print(paste0("exp_",exposures[exp],"_model_",outt,"_",lib))

          zein <- paste0("ATE_",lib,"_",exposures[exp],"_",m)
          print(zein)
          datos <- as.data.frame(cbind(paste0("Exposure-response ",m),exposures[exp],mean(lst[[zein]]),quantile(lst[[zein]],0.025),quantile(lst[[zein]],0.975)))
          names(datos) <- names(get(izena))
          assign(izena,rbind (get(izena), datos))

        } # for exp

        zein2 <- as.data.frame(get(izena))
        # zein2 <- as.data.frame(get(izena))
        zein2$Algorithm<- lib
        names(zein2)[6]<-"X6"
        assign(handia,rbind (get(handia), zein2))

      }
    } # else

    assign("ATE",rbind (get("ATE"), get(handia)))

  } # for(lib)


  ATEs <- ATE[complete.cases(ATE),]


  names(ATEs)=c("ER","Exposure","ATE","LCI","UCI","Algorithm")

  return(list(ATEs,lst,lst2,ATEs_,ATEs_gam))

  write.table(ATEs,paste(fecha,"_ATEs_new2.xls"),sep="\t",row.names = F)

  ptm2=proc.time()-ptm1
  print(ptm2)


}

#' Create ATE
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of libraries to use
#' @param outcomes a vector of the outcomes
#' @param num.sim number of simulations
#' @param plotate boolean to decide if we want to plot
#' @return each of the empty vectors
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

ate_calculate_step12 <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                          confounders = c("sex"), libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"),
                          outcomes = c("Y1","Y2","Y3","Y4"),
                          num.sim = 3, plotate = FALSE,secuence=c(0,1))

{

  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }


  ptm1=proc.time()
  # preparing variables
  len_libraries <- length(libraries)
  if(len_libraries==0)
  {
    simple_libr <- c("")
  } else {
    simple_libr <- gsub("SL.","",libraries)
    simple_libr <- substr(gsub("\\.","",simple_libr),1,5)
    simple_libr <- c("",simple_libr)
  }
  len_libraries <- length(libraries)
  len_exp <- length(exposures)
  len_cof <- length(confounders)
  var_tot <- c(exposures,confounders)
  len_tot <- len_exp + len_cof
  len_out <- length(outcomes)

  mid_data <- dataset[,c(exposures,confounders)]
  d=dataset

  # Create vectors with empty data
  lst <- list()
  lst2 <- list()

  # per each library
  for(lib in simple_libr)
  {
    # per each outcome
    for(ou in 1:len_out)
    {
      # per each exposure
      for(ex in 1:len_exp)
      {
        nam <- paste0("ATE_",lib,"_",exposures[ex],"_",ou)
        lst[[nam]] <- assign(nam, rep(NA, num.sim))

      }

    }
  }

  # Per each simulation all the process
  N <- dim(dataset)[1]
  for(i in 1:num.sim)
  {
    # i <- 1
    print(paste0("i:",i))


    # To take different values for each simulation
    idx=sample(1:N,N,replace=T)
    dataset=d[idx,]


    n_sek <- length(secuence)
    newData <-rep(NA, len_tot)
    len_exp2 <- len_exp*n_sek

    # We create a copy of database, so then we can change values
    for(bat in 1:len_exp2)
    {
      newData <- rbind(newData,mid_data)
    }
    newData <- newData[-1,]



    # put values of the secuence (0 and 1 for example)

    h <- 0
    n <- 1
    for(bat in 1:len_exp)
    {
      for(bi in 1:n_sek)
      {
        h <- h + 1
        n2 <- n + N
        newData[n:(n2-1),bat]<-secuence[bi]
        n <- n2
      }
    }
    # names(newData) <- var_tot


  } # for(i)

  return(list(lst,newData))

}


#' Create rd
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of librariess to use
#' @param outcomes a vector of the outcomes
#' @param num.sim number of simulations
#' @param plotate boolean to decide if we want to plot
#' @param secuence The sequence we need to analyse
#' @return each of the empty vectors
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

rd_calculate <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfda"),
                         confounders = c("sex"), libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"),
                         outcomes = c("Y1","Y2","Y3","Y4"),
                         num.sim = 1,secuence = seq(0,1,0.1))

{
  ptm1=proc.time()
  N <- dim(dataset)[1]
  idx=sample(1:N,N,replace=T)
  dataset=dataset[idx,]

  # exposures <- c("pcb","pfos","hcb","ppdde","pfda")
  # confounders = c("sex")
  # outcomes <- c("Y1","Y2","Y3","Y4")
  # libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest")

  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }

  len_libraries <- length(libraries)
  if(len_libraries==0)
  {
    simple_libr <- c("")
  } else {
    simple_libr <- gsub("SL.","",libraries)
    simple_libr <- substr(gsub("\\.","",simple_libr),1,5)
    simple_libr <- c("",simple_libr)
  }
  len_libraries <- length(libraries)
  len_exp <- length(exposures)
  len_cof <- length(confounders)
  var_tot <- c(exposures,confounders)
  len_tot <- len_exp + len_cof
  len_out <- length(outcomes)

  # dataset <- dataset[,var_tot]

  e <- 1
  s <- 0.05

  sekuentzia <- secuence
  len_sek <- length(sekuentzia)

  #Models#
  #PCB#

  quar <- paste0("newData")
  assign(quar,data.frame(matrix(NA,1,len_tot)))

  len_sek_exp <- len_sek * len_exp

  for(r in 1:len_sek)
  {
    print(r)
    tarteko <- get("newData")
    names(tarteko) <- names(dataset[,var_tot])
    assign(quar, rbind(tarteko,dataset[,var_tot]))
  }

  assign(quar, get(quar)[-1,])


  # calculo de percentiles
  per_value <- NA
  h <- 0
  for(s in 1:len_exp)
  {
    print(exposures[s])
    for(f in sekuentzia)
    {
      h <- h + 1
      print(h)
      valor <- dataset[,exposures[s]]
      per_value <- c(per_value,as.numeric(quantile(valor,f)))
    }

  }
  per_value <- per_value[-1]

  # vec sortu
  h <- 0
  n <- 1
  vec <- n
  for(bat in 1:len_sek)
  {
    n2 <- n + N
    #300
    vec <- c(vec, (n2-1))
    # newData[n:(n2-1),bat]<-1
    # 301
    n <- n2
    vec <- c(vec, (n2))
    # n2 <- n + N
    # #
    # vec <- c(vec, (n2-1))
    # # newData[n:(n2-1),bat]<-0
    # n <- n2
    # vec <- c(vec, n)
  }
  vec <- vec[-length(vec)]

  # asignacion de percentiles
  h <- 0
  for(s in 1:len_exp)
  {
    quar1 <- paste0("newData_",exposures[s])
    tarteko <- get("newData")
    names(tarteko) <- names(dataset[,var_tot])
    assign(quar1,tarteko)

    g <- 0
    h <- 0

    for(f in 1:len_sek)
    {
      print(f)
      g <- g + 1
      h1 <- h + 1
      h2 <- h1 + 1

      tarteko2 <- get(quar1)
      tarteko2[vec[h1]:vec[h2],exposures[s]]<-per_value[g]
      assign(quar1, tarteko2)

      h <- h2

    }

  }


  per <- rep(NA,N)
  len_last <- len_exp * len_out * len_sek
  # rep(exposures,len_out * len_sek)

  last_table <- data.frame(matrix(NA,len_last,4))
  names(last_table) <- c("Exposure","Outcome","Sequence","Value")

  l <- 0
  # creating last table
  for(exp in 1:len_exp)
  {
    for(outc in 1:len_out)
    {
      for(sek in 1:len_sek)
      {
        l <- l + 1
        last_table[l,"Exposure"]<- exposures[exp]
        last_table[l,"Outcome"]<- outcomes[outc]
        last_table[l,"Sequence"]<- secuence[sek]
      }
    }
  }
  l2 <- 0
  for(exp in 1:len_exp)
  {
    # exp <- 1
    print(exposures[exp])
    for(outc in 1:len_out)
    {
      # outc <- 1
      nam <- paste0("SL_fit_",outcomes[outc],"_",exposures[exp])
      tarteko3 <- get(paste0("newData_",exposures[exp]))
      assign(nam,
             SuperLearner(Y=dataset[,outcomes[outc]], X=dataset[,var_tot], SL.library=libraries,
                          family="gaussian",method="method.NNLS", newX=tarteko3[,var_tot], verbose=FALSE))

      tarteko <- get(nam)
      tarteko_rd <- rep(NA,len_sek)
      h <- 0
      for(sek in 1:len_sek)
      {
        # sek <- 1
        l2 <- l2 +1
        # Y0.pcb.pred1 <- SL.fit1.pcb$SL.predict[1:300]

        # print(sek)
        g <- g + 1
        h1 <- h + 1
        h2 <- h1 + 1
        print(paste0(h1,"-",h2))

        last_table[l2,"Value"]<- mean(tarteko$SL.predict[vec[h1]:vec[h2]])

        h <- h2

      }

      # new_name <- paste0("Y",sek,"_",exposures[exp],"_pred",outc)
      # write.table(get(izena3),paste0(izena3,"_RD_.xls"),sep="\t",row.names = F)

    }

  }

  ptm2=proc.time()-ptm1
  print(ptm2)

  save.image("20171122_RD.RData")
  return(last_table)
}


#' Create ICE
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of librariess to use
#' @param outcomes a vector of the outcomes
#' @param num.sim number of simulations
#' @param plotate boolean to decide if we want to plot
#' @return each of the empty vectors
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

ice_calculate <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                          confounders = c("sex"), libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"),
                          outcomes = c("Y1","Y2","Y3","Y4"),
                          num.sim = 3,secuence = seq(0,1,0.05))

{
  ptm1=proc.time()

  # exposures <- c("pcb","pfos","hcb","ppdde","pfda")
  # confounders = c("sex")
  # outcomes <- c("Y1","Y2","Y3","Y4")
  # libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest")

  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }

  len_libraries <- length(libraries)
  if(len_libraries==0)
  {
    simple_libr <- c("")
  } else {
    simple_libr <- gsub("SL.","",libraries)
    simple_libr <- substr(gsub("\\.","",simple_libr),1,5)
    simple_libr <- c("",simple_libr)
  }
  len_libraries <- length(libraries)
  len_exp <- length(exposures)
  len_cof <- length(confounders)
  var_tot <- c(exposures,confounders)
  len_tot <- len_exp + len_cof
  len_out <- length(outcomes)


  N <- dim(dataset)[1]
  # dataset <- dataset[,var_tot]

  e <- 1
  s <- 0.05

  sekuentzia <- secuence
  len_sek <- length(sekuentzia)

  #Models#
  #PCB#

  quar <- paste0("newData")
  assign(quar,data.frame(matrix(NA,1,len_tot)))

  len_sek_exp <- len_sek * len_exp

  for(r in 1:len_sek)
  {
    print(r)
    tarteko <- get("newData")
    names(tarteko) <- names(dataset[,var_tot])
    assign(quar, rbind(tarteko,dataset[,var_tot]))
  }

  assign(quar, get(quar)[-1,])


  # calculo de percentiles
  per_value <- NA
  h <- 0
  for(s in 1:len_exp)
  {
    print(exposures[s])
    for(f in sekuentzia)
    {
      h <- h + 1
      print(h)
      valor <- dataset[,exposures[s]]
      per_value <- c(per_value,as.numeric(quantile(valor,f)))
    }

  }
  per_value <- per_value[-1]

  # vec sortu
  h <- 0
  n <- 1
  vec <- n
  for(bat in 1:len_sek)
  {
    n2 <- n + N
    #300
    vec <- c(vec, (n2-1))
    # newData[n:(n2-1),bat]<-1
    # 301
    n <- n2
    vec <- c(vec, (n2))
    # n2 <- n + N
    # #
    # vec <- c(vec, (n2-1))
    # # newData[n:(n2-1),bat]<-0
    # n <- n2
    # vec <- c(vec, n)
  }
  vec <- vec[-length(vec)]

  # asignacion de percentiles
  h <- 0
  for(s in 1:len_exp)
  {
    quar1 <- paste0("newData_",exposures[s])
    tarteko <- get("newData")
    names(tarteko) <- names(dataset[,var_tot])
    assign(quar1,tarteko)

    g <- 0
    h <- 0

    for(f in 1:len_sek)
    {
      print(f)
      g <- g + 1
      h1 <- h + 1
      h2 <- h1 + 1

      tarteko2 <- get(quar1)
      tarteko2[vec[h1]:vec[h2],exposures[s]]<-per_value[g]
      assign(quar1, tarteko2)

      h <- h2

    }

  }


  per <- rep(NA,N)

  #####################################
  ### last table
  #####################################

  len_last <- len_exp * len_out * len_sek * N
  # rep(exposures,len_out * len_sek)

  # last_table <- data.frame(matrix(NA,len_last,4))
  # names(last_table) <- c("Exposure","Outcome","Sequence","Value")
  len_ind <- len_exp * len_out * len_sek

  #exp
  exp2 <- NA
  for(expg in 1:len_exp)
  {
    exp2 <- c(exp2,rep(exposures[expg],N*len_sek*len_out))
  }
  # last_table$Outcome <- exp2
  exp2 <- exp2[-1]

  #out
  out2 <- NA
  for(outg in 1:len_out)
  {

    out2 <- c(out2,rep(outcomes[outg],N*len_sek))
  }
  out2 <- out2[-1]
  out3 <- rep(out2,len_exp)
  # last_table$Outcome <- out2

  #sek
  sek2 <- NA
  for(sek in 1:len_sek)
  {
    sek2 <- c(sek2,rep(secuence[sek],N))
  }
  sek2 <- sek2[-1]
  sek3 <- rep(sek2,len_exp * len_out)
  # last_table$Sequence <- sek2
  #ind
  last_table <- data.frame(Individuo=rep(1:N,len_ind),Sequence=sek2,Output=out3,Exposure=exp2)
  last_table$Value <- 999

  N2 <- dim(newData)[1]
  non <- NA
  l2 <- 0
  w <- 0
  for(exp in 1:len_exp)
  {
    # exp <- 1
    print(exposures[exp])
    for(outc in 1:len_out)
    {
      # outc <- 1
      print(outcomes[outc])
      nam <- paste0("SL_fit_",outcomes[outc],"_",exposures[exp])
      tarteko3 <- get(paste0("newData_",exposures[exp]))
      assign(nam,
             SuperLearner(Y=dataset[,outcomes[outc]], X=dataset[,var_tot], SL.library=libraries,
                          family="gaussian",method="method.NNLS", newX=tarteko3[,var_tot], verbose=FALSE))

      tarteko <- get(nam)
      h <- 0
      for(sek in 1:len_sek)
      {
        # sek <- 1
        # for(ind in 1:N)
        # {

        # Y0.pcb.pred1 <- SL.fit1.pcb$SL.predict[1:300]

        l2 <- l2 +1
        # Y0.pcb.pred1 <- SL.fit1.pcb$SL.predict[1:300]

        # print(sek)
        g <- g + 1
        h1 <- h + 1
        h2 <- h1 + 1

        non <- (vec[h1]+w):(vec[h2]+w)
        print(paste0(vec[h1]+w,"-",vec[h2]+w))
        last_table[non,"Value"]<- tarteko$SL.predict[vec[h1]:vec[h2]]

        h <- h2
        # }


      }
      w <- w + N2

      # assign(paste0("ICE",outc,"_",exposures[exp]),per[,-1])
      # write.table(get(izena3),paste0(izena3,".xls"),sep="\t",row.names = F)

    }

  }

  ptm2=proc.time()-ptm1
  print(ptm2)

  # save.image("ICE.RData")
  return(last_table)
}




#' Interaction function
#'
#' @param dataset dataset with all variables
#' @param exposures a vector with exposures
#' @param confounders a vector with confounders
#' @param libraries a vector of librariess to use
#' @param outcomes a vector of the outcomes
#' @param num.sim number of simulations
#' @param plotate boolean to decide if we want to plot
#' @return each of the empty vectors
#' @export
#' @details libraries could be SL if we don't select nothing or "SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"

interaction_f <- function(dataset = data.X2, exposures = c("pcb","pfos","hcb","ppdde","pfoa"),
                          confounders = c("sex"), libraries = c("SL.glm", "SL.glm.interaction","SL.glmnet", "SL.gam","SL.xgboost","SL.polymars","SL.randomForest"),
                          outcomes = c("Y1","Y2","Y3","Y4"),
                          num.sim = 3)

{

  if(class(exposures)%in%c("numeric","integer")) {  exposures <- names(dataset[,exposures]) }
  if(class(confounders)%in%c("numeric","integer")) {  confounders <- names(dataset[,confounders]) }
  if(class(outcomes)%in%c("numeric","integer")) {  outcomes <- names(dataset[,outcomes]) }

  len_libraries <- length(SL.library)
  simple_libr <- gsub("SL.","",SL.library)
  simple_libr <- substr(gsub("\\.","",simple_libr),1,5)
  simple_libr <- c("",simple_libr)
  expo_cof <- c(exposures,cof)
  len_exp <- length(exposures)
  len_expo_cof <- length(expo_cof)
  len_out <- length(outcomes)

  num.sim = 2

  #Creating final dataset
  tot_pos <- NA
  ec <- 1

  zenb <- 0
  for(ex in 1:len_expo_cof)
  {
    # print(exposures[ex])
    ec <- ec + 1
    for(ex2 in ec:len_expo_cof)
    {
      zenb <- zenb + 1
      print(paste0(expo_cof[ex],"-",expo_cof[ex2]))
      tot_pos <- c(tot_pos,expo_cof[ex],expo_cof[ex2])
    }
  }

  final_table <- data.frame(matrix(NA,zenb,5))
  names(final_table) <- c("Exp1","Exp2","Sim","Outcome","Value")

  ec <- 1
  zenb <- 0
  for(ex in 1:len_expo_cof)
  {
    # print(exposures[ex])
    ec <- ec + 1
    for(ex2 in ec:len_expo_cof)
    {
      zenb <- zenb + 1
      final_table[zenb,"Exp1"] <- expo_cof[ex]
      final_table[zenb,"Exp2"] <- expo_cof[ex2]
    }
  }



  N <- dim(dataset)[1]

  # out
  out2 <- NA
  for(outg in 1:len_out)
  {

    out2 <- c(out2,rep(outcomes[outg],zenb))
  }
  out2 <- out2[-1]
  out3 <- rep(out2,num.sim)

  # sim
  sim2 <- NA
  for(simg in 1:num.sim)
  {

    sim2 <- c(sim2,rep(simg,zenb*len_out))
  }
  sim2 <- sim2[-1]


  final_table2 <- data.frame(rep(final_table$Exp1,len_out*num.sim),
                             rep(final_table$Exp2,len_out*num.sim),out3,sim2)
  final_table2$Value <- 999

  names(final_table2) <- c("Exp1","Exp2","Sim","Outcome","Value")
  tot_dim <- dim(final_table2)[1]


  zeintzuk <- 0
  # num.sim = 3
  ptm1=proc.time()
  for(i in 1:num.sim)
  {
    # i <- 1
    idx=sample(1:N,N,replace=T)
    data.X2=d[idx,]

    # each interaction
    mid_data <- data.X2[,c(expo_cof)]

    ec <- 1
    for(ex in 1:len_expo_cof)
    {
      # ex <- 1
      # print(ex)
      ec <- ec + 1
      if(len_expo_cof==ex) break
      for(ex2 in ec:len_expo_cof)
      {
        # ex2 <- 2
        # print(ex2)
        print(paste0(expo_cof[ex],"-",expo_cof[ex2]))

        for(ou in 1:len_out)
        {
          zeintzuk <- zeintzuk + 1
          print(paste0("Register: ",zeintzuk, " de ",tot_dim, " -> ", Sys.time()))
          # ou <- 1
          # print(ou)
          namtt <- paste0("INT_",ou,"_",expo_cof[ex],"_x_",expo_cof[ex2])
          nam <- paste0("INT_",ou,"_",expo_cof[ex],"_x_",expo_cof[ex2])
          nam2 <- paste0(expo_cof[ex],"_x_",expo_cof[ex2])
          print(nam)
          assign(nam, rep(NA, num.sim), envir = parent.frame())

          # generamos las 4 copias de la base de datos
          newData <- rep(NA, len_expo_cof)

          for(bat in 1:2)
          {
            newData <- rbind(newData,mid_data)
            newData <- rbind(newData,mid_data)
          }

          newData <- newData[-1,]

          # newData.pcbXpfos <- rbind(cbind(pcb=1,pfos=1, data.X2[,3:6]),
          #                           cbind(pcb=1,pfos=0, data.X2[,3:6]),
          #                           cbind(pcb=0,pfos=1, data.X2[,3:6]),
          #                           cbind(pcb=0,pfos=0, data.X2[,3:6]))

          # poner 0 y 1
          # hemen zeoze arraro dago
          h <- 0
          n <- 1
          n_sek <- length(secuence)
          for(bat in c(ex,ex2))
          {
            h <- h + 1
            n2 <- n + N
            newData[n:(n2-1),bat]<-secuence[1]
            n <- n2
            n2 <- n + N
            h <- h + 1
            newData[n:(n2-1),bat]<-secuence[2]
            n <- n2
          }

          # for(bat in 1:len_exp2)
          # {
          #   for(bi in 1:n_sek)
          #   {
          #     h <- h + 1
          #     n2 <- n + N
          #     newData[n:(n2-1),bat]<-secuence[bi]
          #     n <- n2
          #   }
          # }

          nam <- paste0("SL.fit_",outcomes[ou],"_",nam2)



          assign(nam,
                 SuperLearner(Y=data.X2[,outcomes[ou]], X=data.X2[,expo_cof], SL.library=SL.library,
                              family="gaussian",method="method.NNLS", newX=newData, verbose=FALSE)
                 , envir = parent.frame())


          # SL.fit1.pcbXpfos <- SuperLearner(Y=data.X2[,outcomes[outc]], X=data.X2[,1:6], SL.library=SL.library,
          #                                  family="gaussian",method="method.NNLS", newX=newData.pcbXpfos, verbose=TRUE)

          # poner 0 y 1
          h <- 0
          n1<- 1

          h <- h + 1
          n2 <- n1 + N
          newData[n1:(n2-1),bat]<-1

          pre <- paste0("Y11_pred",ou)
          ekarri <- get(nam)
          assign(pre,
                 ekarri$SL.predict[n1:(n2-1)])


          n <- n2
          n2 <- n + N
          h <- h + 1

          pre <- paste0("Y10_pred",ou)
          ekarri <- get(nam)
          assign(pre,
                 ekarri$SL.predict[n1:(n2-1)])

          n <- n2
          n2 <- n + N
          h <- h + 1

          pre <- paste0("Y01_pred",ou)
          ekarri <- get(nam)
          assign(pre,
                 ekarri$SL.predict[n1:(n2-1)])

          n <- n2
          n2 <- n + N
          h <- h + 1

          pre <- paste0("Y00_pred",ou)
          ekarri <- get(nam)
          assign(pre,
                 ekarri$SL.predict[n1:(n2-1)])

          n <- n2



          # ekarri <- get(namtt)
          final_table2[zeintzuk,"Value"] <- mean(Y11_pred1 - Y10_pred1 - Y01_pred1 + Y00_pred1)
          # assign(namtt, ekarri)

        }
      }


    }
  }

  ptm2=proc.time()-ptm1
  # save.image("20171121_Interaction.RData")
}
